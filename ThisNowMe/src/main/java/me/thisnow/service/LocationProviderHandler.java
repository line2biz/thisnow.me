package me.thisnow.service;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;

import me.thisnow.service.util.Payload;
import me.thisnow.util.FusedLocation;
import me.thisnow.util.Preferences;

import static me.thisnow.service.constant.Action.LOCATION_SYNC;
import static me.thisnow.service.constant.Path.SYNC;
import static me.thisnow.util.FusedLocation.UpdateListener;

public final class LocationProviderHandler extends Service implements UpdateListener {
    private static final String TAG = LocationProviderHandler.class.getSimpleName();
    private String mAccountToken = null;

    @Override
    public void onCreate() {
        super.onCreate();
        mAccountToken = Preferences.getAccountToken(getApplicationContext());
        FusedLocation.getInstance().checkGooglePlayServices(getApplicationContext());
        FusedLocation.getInstance().connect(getApplicationContext(), this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        FusedLocation.getInstance().disconnect(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onChange(Location location) {
        if (mAccountToken != null) {
            AppServerHandler.post(getApplicationContext(), LOCATION_SYNC, SYNC,
                    Payload.getInstance().getForSync(mAccountToken, location.getLatitude(), location.getLongitude()));
        }
    }

}