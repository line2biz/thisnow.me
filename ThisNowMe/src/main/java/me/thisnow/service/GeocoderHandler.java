package me.thisnow.service;

import android.content.Context;

import com.google.gson.ExclusionStrategy;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import me.thisnow.R;
import me.thisnow.service.model.geocoding.Response;
import me.thisnow.singleton.GeocoderHolder;
import me.thisnow.service.strategy.All;

import static me.thisnow.service.constant.Action.GET_ADDRESS_LOCATION;
import static me.thisnow.service.constant.Authority.GOOGLE_MAPS_API;
import static me.thisnow.service.constant.Method.GET;
import static me.thisnow.service.constant.Path.GEOCODING;
import static me.thisnow.service.constant.Protocol.HTTP;
import static me.thisnow.service.ResponseCode.SERVER_CONNECTION_ERROR;
import static me.thisnow.service.GeocoderHandler.Status.UNKNOWN_ERROR;


public final class GeocoderHandler extends AbstractServerHandler<Response> {
    private static final String TAG = GeocoderHandler.class.getSimpleName();

    public GeocoderHandler() {
        super(TAG);
    }

    public static void getLocationFromAddress(Context context, String address) {
        try {
            request(context, GeocoderHandler.class, GET_ADDRESS_LOCATION, GET, GEOCODING + URLEncoder.encode(address, UTF_8), null);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Response buildErrorResponse() {
        Response response = new Response();
        response.setStatus(UNKNOWN_ERROR);
        return response;
    }

    @Override
    protected String getProtocol() {
        return HTTP;
    }

    @Override
    protected String getHost() {
        return GOOGLE_MAPS_API;
    }

    @Override
    protected void handleResponse(Response response, String action) {
        if (response.getResults() != null) {
            if (GET_ADDRESS_LOCATION.equals(action)) {
                GeocoderHolder.getInstance().setResults(response.getResults());
            }
        }
    }

    @Override
    protected ExclusionStrategy getExclusionStrategy(String action) {
        return new All();
    }

    @Override
    protected Class<Response> getCls() {
        return Response.class;
    }

    @Override
    protected int getCode(Response response) {
        return Status.OK.equals(response.getStatus()) ? ResponseCode.OK : SERVER_CONNECTION_ERROR;
    }

    @Override
    protected String getMessage(Response response) {
        return Status.OK.equals(response.getStatus()) ? null : getString(R.string.error_server_problem);
    }

    interface Status {
        String OK = "OK";
        String UNKNOWN_ERROR = "UNKNOWN_ERROR";
    }

}