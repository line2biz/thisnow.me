package me.thisnow.service.constant;

public interface Authority {
    String THIS_NOW_ME = "thisnowme.automatic.com.ua";
    String GOOGLE_MAPS_API = "maps.googleapis.com";
}