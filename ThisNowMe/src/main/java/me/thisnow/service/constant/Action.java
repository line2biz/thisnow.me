package me.thisnow.service.constant;

public interface Action {
    String BASE = Action.class.getCanonicalName();

    String GET_PERSON_SUBTYPES = BASE + ".GetSubtypes.Person";
    String GET_BUSINESS_SUBTYPES = BASE + ".GetSubtypes.Business";
    String GET_EVENT_SUBTYPES = BASE + ".GetSubtypes.Event";
    String REGISTER_PERSON = BASE + ".Register.Person";
    String REGISTER_BUSINESS = BASE + ".Register.Business";
    String REGISTER_EVENT = BASE + ".Register.Event";
    String LOGIN_AUTH = BASE + ".Login.Auth";
    String LOGIN_NO_AUTH = BASE + ".Login.NoAuth";
    String LOGOUT = BASE + ".Logout";
    String AMAZON_UPLOAD = BASE + ".Amazon.Upload";
    String MEDIA_UPLOAD = BASE + ".Media.Upload";
    String LOCATION_SYNC = BASE + ".Location.Sync";
    String GET_TIMELINE_PERSON_OBJECTS = BASE + ".GetObjects.Timeline.Person";
    String GET_TIMELINE_BUSINESS_OBJECTS = BASE + ".GetObjects.Timeline.Business";
    String GET_TIMELINE_EVENT_OBJECTS = BASE + ".GetObjects.Timeline.Event";
    String GET_AROUND_ME_PERSON_OBJECTS = BASE + ".GetObjects.Around.Me.Person";
    String GET_AROUND_ME_BUSINESS_OBJECTS = BASE + ".GetObjects.Around.Me.Business";
    String GET_AROUND_ME_EVENT_OBJECTS = BASE + ".GetObjects.Around.Me.Event";

    String GET_ADDRESS_LOCATION = BASE + ".GetAddressLocation";

    String RESPONSE = BASE + ".Response";
}