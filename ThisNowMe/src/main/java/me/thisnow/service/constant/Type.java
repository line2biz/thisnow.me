package me.thisnow.service.constant;

public interface Type {
    int ABSENT = -1;
    int PERSON = 100;
    int BUSINESS = 200;
    int EVENT = 300;
}