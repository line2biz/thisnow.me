package me.thisnow.service.constant;

public interface Tab {

    public interface Title {
        String TIMELINE = "Timeline";
        String AROUND_ME = "Around Me";
        String SETTINGS = "Settings";
        String EVENTS = "Events";
        String CHAT = "Chat";
    }

    public interface Tag {
        String BASE = Tag.class.getCanonicalName();
        String TIMELINE = BASE + ".Timeline";
        String AROUND_ME = BASE + ".Around.Me";
        String SETTINGS = BASE + ".Settings";
        String EVENTS = BASE + ".Events";
        String CHAT = BASE + ".Chat";
    }

}