package me.thisnow.service.constant;

public interface Path {
    String SERVICE = "service/";
    String TYPE_BASE = SERVICE + "object_subtypes/type/";
    String REGISTER = SERVICE + "register";
    String AUTH = SERVICE + "login";
    String NO_AUTH = SERVICE + "login_anonymous";
    String LOGOUT = SERVICE + "logout";
    String MEDIA_SAVE = SERVICE + "media_save";
    String SYNC = SERVICE + "sync";
    String OBJECTS_LIST = SERVICE + "object_list";

    String GEOCODING = "maps/api/geocode/json?sensor=true&address=";
}