package me.thisnow.service.constant;

public interface Mode {
    int TIMELINE = 0x4301;
    int AROUND_ME = 0x4302;
}