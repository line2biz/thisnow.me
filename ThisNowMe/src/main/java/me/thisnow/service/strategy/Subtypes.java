package me.thisnow.service.strategy;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

import me.thisnow.service.model.Data;

import static me.thisnow.service.strategy.DataFields.OBJECTS_LIST;
import static me.thisnow.service.strategy.DataFields.OBJECT_ID;
import static me.thisnow.service.strategy.DataFields.OBJECT_TYPE;
import static me.thisnow.service.strategy.DataFields.TOKEN;

public final class Subtypes implements ExclusionStrategy {

    @Override
    public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        return (fieldAttributes.getDeclaringClass() == Data.class && fieldAttributes.getName().equals(OBJECTS_LIST))
                || (fieldAttributes.getDeclaringClass() == Data.class && fieldAttributes.getName().equals(TOKEN))
                || (fieldAttributes.getDeclaringClass() == Data.class && fieldAttributes.getName().equals(OBJECT_ID))
                || (fieldAttributes.getDeclaringClass() == Data.class && fieldAttributes.getName().equals(OBJECT_TYPE));
    }

    @Override
    public boolean shouldSkipClass(Class<?> aClass) {
        return false;
    }

}