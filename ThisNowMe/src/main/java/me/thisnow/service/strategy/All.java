package me.thisnow.service.strategy;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

public final class All implements ExclusionStrategy {

    @Override
    public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        return false;
    }

    @Override
    public boolean shouldSkipClass(Class<?> aClass) {
        return false;
    }

}