package me.thisnow.service.strategy;

interface DataFields {
    String OBJECTS_LIST = "objectsList";
    String SUBTYPES = "subtypes";
    String TOKEN = "token";
    String OBJECT_ID = "objectId";
    String OBJECT_TYPE = "objectType";
    String DATA = "data";
}