package me.thisnow.service.strategy;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

import me.thisnow.service.model.Data;
import me.thisnow.service.model.Response;

import static me.thisnow.service.strategy.DataFields.DATA;
import static me.thisnow.service.strategy.DataFields.OBJECTS_LIST;

public final class MediaSave implements ExclusionStrategy {

    @Override
    public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        return (fieldAttributes.getDeclaringClass() == Data.class && fieldAttributes.getName().equals(OBJECTS_LIST))
                || (fieldAttributes.getDeclaringClass() == Response.class && fieldAttributes.getName().equals(DATA));
    }

    @Override
    public boolean shouldSkipClass(Class<?> aClass) {
        return false;
    }

}