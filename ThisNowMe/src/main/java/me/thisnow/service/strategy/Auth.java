package me.thisnow.service.strategy;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

import me.thisnow.service.model.Data;

import static me.thisnow.service.strategy.DataFields.OBJECTS_LIST;
import static me.thisnow.service.strategy.DataFields.SUBTYPES;

public final class Auth implements ExclusionStrategy {

    @Override
    public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        return (fieldAttributes.getDeclaringClass() == Data.class && fieldAttributes.getName().equals(OBJECTS_LIST))
                || (fieldAttributes.getDeclaringClass() == Data.class && fieldAttributes.getName().equals(SUBTYPES));
    }

    @Override
    public boolean shouldSkipClass(Class<?> aClass) {
        return false;
    }

}