package me.thisnow.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.format.DateUtils;
import android.util.Log;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.google.gson.JsonSyntaxException;
import com.readystatesoftware.simpl3r.UploadIterruptedException;
import com.readystatesoftware.simpl3r.Uploader;
import com.squareup.okhttp.OkHttpClient;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import me.thisnow.R;
import me.thisnow.database.model.Attachment;
import me.thisnow.service.util.Payload;
import me.thisnow.singleton.UploadAttachmentsHolder;
import me.thisnow.util.Encrypt;

import static java.net.HttpURLConnection.HTTP_OK;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static me.thisnow.constant.Char.AND;
import static me.thisnow.constant.Char.EQUAL;
import static me.thisnow.service.Extra.ACTION;
import static me.thisnow.service.Extra.CODE;
import static me.thisnow.service.Extra.MESSAGE;
import static me.thisnow.service.ResponseCode.OK;
import static me.thisnow.service.ResponseCode.SERVER_CONNECTION_ERROR;
import static me.thisnow.service.constant.Action.AMAZON_UPLOAD;
import static me.thisnow.service.constant.Action.RESPONSE;
import static me.thisnow.service.constant.Authority.THIS_NOW_ME;
import static me.thisnow.service.constant.Method.POST;
import static me.thisnow.service.constant.Path.MEDIA_SAVE;
import static me.thisnow.service.constant.Protocol.HTTP;

public final class UploadAttachmentHandler extends IntentService {
    private static final String TAG = UploadAttachmentHandler.class.getSimpleName();
    private static final String UTF_8 = "UTF-8";
    private static final long CONNECTION_TIMEOUT = 5 * DateUtils.SECOND_IN_MILLIS;
    private static final long READ_TIMEOUT = 15 * DateUtils.SECOND_IN_MILLIS;
    private final OkHttpClient httpClient;
    private AmazonS3Client amazonS3Client;
    private BroadcastEvent broadcastEvent;
    private String bucketName;
    private String uploadInterrupted;

    public UploadAttachmentHandler() {
        super(TAG);
        httpClient = new OkHttpClient();
        httpClient.setConnectTimeout(CONNECTION_TIMEOUT, MILLISECONDS);
        httpClient.setReadTimeout(READ_TIMEOUT, MILLISECONDS);
    }

    public static void upload(Context context) {
        Intent intent = new Intent(context, UploadAttachmentHandler.class);
        intent.setAction(AMAZON_UPLOAD);
        context.startService(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        amazonS3Client = new AmazonS3Client(new BasicAWSCredentials(getString(R.string.access_key), getString(R.string.secret_key)));
        bucketName = getString(R.string.bucket_name);
        uploadInterrupted = getString(R.string.error_upload_interrupted);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            initBroadcastEvent(action);
            if (AMAZON_UPLOAD.equals(action)) {
                final ArrayList<Attachment> attachments = getAttachments();
                if (attachments.size() > 0) {
                    UploadAttachmentsHolder.getInstance().setMax(attachments.size());
                    uploadAttachments(attachments, action);
                }
                sendLocalBroadcast();
            }
        }
    }

    private void initBroadcastEvent(String action) {
        broadcastEvent = new BroadcastEvent();
        broadcastEvent.setAction(action);
        broadcastEvent.setMessage(null);
        broadcastEvent.setCode(OK);
    }

    private void uploadAttachments(ArrayList<Attachment> attachments, String action) {
        OutputStream output = null;
        try {
            for (int i = 0; i < attachments.size(); i++) {
                checkStopValue();
                final String url = uploadAttachmentToAmazon(attachments.get(i).getPath());
                if (url != null) {
                    Log.d(TAG, "upload: amazon S3 server: url " + url);
                    HttpURLConnection connection = httpClient.open(new URL(HTTP, THIS_NOW_ME, MEDIA_SAVE));
                    connection.setRequestMethod(POST);
                    byte[] payload = getPayloadBytes(Payload.getInstance().getForAttachment(url, attachments.get(i).getMediaType()));
                    if (payload != null) {
                        connection.setDoOutput(true);
                        output = connection.getOutputStream();
                        output.write(payload);
                        output.flush();
                        output.close();
                    }
                    checkResponseCode(connection);
                    UploadAttachmentsHolder.getInstance().setCurrent(i + 1);
                    UploadAttachmentsHolder.getInstance().deleteById(getApplicationContext(), attachments.get(i).getId());
                } else {
                    throw new IOException("URL from Amazon is null!");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            broadcastEvent.setCode(SERVER_CONNECTION_ERROR);
            broadcastEvent.setMessage(uploadInterrupted);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            broadcastEvent.setCode(SERVER_CONNECTION_ERROR);
            broadcastEvent.setMessage(uploadInterrupted);
        } catch (UploadIterruptedException e) {
            e.printStackTrace();
            broadcastEvent.setCode(SERVER_CONNECTION_ERROR);
            broadcastEvent.setMessage(uploadInterrupted);
        } finally {
            close(output);
        }
    }

    private ArrayList<Attachment> getAttachments() {
        ArrayList<Attachment> attachments = new ArrayList<Attachment>();
        Cursor cursor = UploadAttachmentsHolder.getInstance().getAll(getApplicationContext());
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                Attachment attachment = new Attachment();
                attachment.getEntryFromCursor(cursor);
                attachments.add(attachment);
                cursor.moveToNext();
            }
            cursor.close();
        }
        Log.d(TAG, "upload: attachments count: " + attachments.size());
        return attachments;
    }

    private void checkStopValue() throws IOException {
        if (UploadAttachmentsHolder.getInstance().getHandlerStopState()) {
            throw new IOException("Service stopped by user...");
        }
    }

    private String uploadAttachmentToAmazon(String path) {
        amazonS3Client.createBucket(bucketName);
        return new Uploader(getApplicationContext(), amazonS3Client, bucketName, Encrypt.md5(path), new File(path)).start();
    }

    private byte[] getPayloadBytes(Bundle bundle) {
        try {
            StringBuilder builder = new StringBuilder();
            for (String key : bundle.keySet()) {
                if (builder.length() != 0) {
                    builder.append(AND);
                }
                builder.append(key).append(EQUAL).append(bundle.get(key));
            }
            Log.d(TAG, "upload: request params: " + builder.toString());
            return builder.toString().getBytes(UTF_8);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void checkResponseCode(HttpURLConnection connection) throws IOException {
        if (connection.getResponseCode() != HTTP_OK) {
            throw new IOException("Unexpected HTTP response: " + connection.getResponseCode() + " " + connection.getResponseMessage());
        }
    }

    private void close(Closeable stream) {
        try {
            if (stream != null) {
                stream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendLocalBroadcast() {
        UploadAttachmentsHolder.getInstance().setHandlerStopState(false);
        Log.d(TAG, "send broadcast!");
        Intent intent = new Intent(RESPONSE);
        intent.putExtra(ACTION, broadcastEvent.getAction());
        intent.putExtra(CODE, broadcastEvent.getCode());
        intent.putExtra(MESSAGE, broadcastEvent.getMessage());
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    private static class BroadcastEvent {
        private String action;
        private String message;
        private int code;

        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }

}