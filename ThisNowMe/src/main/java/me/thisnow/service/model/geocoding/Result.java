package me.thisnow.service.model.geocoding;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class Result {
    @Expose
    private Geometry geometry;
    @Expose
    @SerializedName("formatted_address")
    private String formattedAddress;

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

}