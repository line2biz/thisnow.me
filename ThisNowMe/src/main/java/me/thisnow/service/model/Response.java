package me.thisnow.service.model;

import com.google.gson.annotations.Expose;

import me.thisnow.service.model.Data;

public final class Response {
    @Expose
    private int code;
    @Expose
    private String message;
    @Expose
    private Data data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}