package me.thisnow.service.model;

import com.google.gson.annotations.Expose;

public final class Subtype {
    @Expose
    private int code;
    @Expose
    private String name;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}