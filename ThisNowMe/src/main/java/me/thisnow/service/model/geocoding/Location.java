package me.thisnow.service.model.geocoding;

import com.google.gson.annotations.Expose;

public final class Location {
    @Expose
    private double lat;
    @Expose
    private double lng;

    public double getLatitude() {
        return lat;
    }

    public void setLatitude(double latitude) {
        this.lat = latitude;
    }

    public double getLongitude() {
        return lng;
    }

    public void setLongitude(double longitude) {
        this.lng = longitude;
    }

}