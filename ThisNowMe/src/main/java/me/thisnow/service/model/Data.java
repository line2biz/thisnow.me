package me.thisnow.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import me.thisnow.singleton.model.ObjectItem;

public final class Data {
    @Expose
    @SerializedName("object_list")
    private ArrayList<ObjectItem> objectsList;
    @Expose
    private ArrayList<Subtype> subtypes;
    @Expose
    private String token;
    @Expose
    @SerializedName("object_id")
    private String objectId;
    @Expose
    @SerializedName("object_type")
    private int objectType;

    public ArrayList<ObjectItem> getObjectsList() {
        return objectsList;
    }

    public void setObjectsList(ArrayList<ObjectItem> objectsList) {
        this.objectsList = objectsList;
    }

    public ArrayList<Subtype> getSubtypes() {
        return subtypes;
    }

    public void setSubtypes(ArrayList<Subtype> subtypes) {
        this.subtypes = subtypes;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public int getObjectType() {
        return objectType;
    }

    public void setObjectType(int objectType) {
        this.objectType = objectType;
    }

}