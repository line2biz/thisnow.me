package me.thisnow.service.model.geocoding;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

public final class Response {
    @Expose
    private ArrayList<Result> results;
    @Expose
    private String status;

    public ArrayList<Result> getResults() {
        return results;
    }

    public void setResults(ArrayList<Result> results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}