package me.thisnow.service.model.geocoding;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class Geometry {
    @Expose
    private Location location;
    @Expose
    @SerializedName("location_type")
    private String locationType;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

}