package me.thisnow.service;

interface ResponseCode {
    int OK = 0;
    int SERVER_CONNECTION_ERROR = -1;
}