package me.thisnow.service.util;

import android.os.Bundle;

import me.thisnow.singleton.AccountInfoHolder;

import static me.thisnow.service.util.Parameter.MEDIA_TYPE;
import static me.thisnow.service.util.Parameter.OBJECT_ID;
import static me.thisnow.service.util.Parameter.TOKEN;
import static me.thisnow.service.util.Parameter.URL;

final class Attachment extends ParametersBuilder {
    private final String url;
    private final String mediaType;

    Attachment(String url, String mediaType) {
        this.url = url;
        this.mediaType = mediaType;
    }

    @Override
    public Bundle putData(Bundle data) {
        data.putString(TOKEN, AccountInfoHolder.getInstance().getToken());
        data.putString(OBJECT_ID, AccountInfoHolder.getInstance().getObjectId());
        data.putString(URL, url);
        data.putString(MEDIA_TYPE, mediaType);
        return data;
    }

}