package me.thisnow.service.util;

import android.os.Bundle;

import static me.thisnow.constant.Char.AND;
import static me.thisnow.constant.Char.EQUAL;
import static me.thisnow.service.util.ParametersBuilder.DefaultValues.DOUBLE;
import static me.thisnow.service.util.ParametersBuilder.DefaultValues.INT;
import static me.thisnow.service.util.ParametersBuilder.DefaultValues.LONG;

public abstract class ParametersBuilder {

    public ParametersBuilder() {

    }

    public abstract Bundle putData(Bundle data);

    protected void putValue(Bundle bundle, String key, String value) {
        if (value != null && !value.isEmpty()) {
            bundle.putString(key, value);
        }
    }

    protected void putValue(Bundle bundle, String key, int value) {
        if (value >= INT) {
            bundle.putInt(key, value);
        }
    }

    protected void putValue(Bundle bundle, String key, long value) {
        if (value >= LONG) {
            bundle.putLong(key, value);
        }
    }

    protected void putValue(Bundle bundle, String key, double value) {
        if (value >= DOUBLE) {
            bundle.putDouble(key, value);
        }
    }

    protected void putValue(Bundle bundle, String key, String... values) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < values.length; i++) {
            if (builder.length() != 0) {
                builder.append(AND).append(key).append(EQUAL);
            }
            builder.append(values[i]);
        }
        bundle.putString(key, builder.toString());
    }

    public interface DismissValues {
        int DISMISS_INT = -1;
        long DISMISS_LONG = -1l;
        double DISMISS_DOUBLE = -1.0d;
    }

    interface DefaultValues {
        int INT = 0;
        long LONG = 0;
        double DOUBLE = 0.0d;
    }

}
