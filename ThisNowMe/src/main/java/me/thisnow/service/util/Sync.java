package me.thisnow.service.util;

import android.os.Bundle;

import me.thisnow.singleton.AccountInfoHolder;

import static me.thisnow.service.util.Parameter.LATITUDE;
import static me.thisnow.service.util.Parameter.LONGITUDE;
import static me.thisnow.service.util.Parameter.TOKEN;

final class Sync extends ParametersBuilder {
    private final String token;
    private final double latitude;
    private final double longitude;

    Sync(String token, double latitude, double longitude) {
        this.token = token;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public Bundle putData(Bundle data) {
        data.putString(TOKEN, token);
        data.putDouble(LATITUDE, latitude);
        data.putDouble(LONGITUDE, longitude);
        return data;
    }

}