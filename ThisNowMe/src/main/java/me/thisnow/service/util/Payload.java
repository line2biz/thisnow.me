package me.thisnow.service.util;

import android.content.Context;
import android.os.Bundle;

import static me.thisnow.service.util.Parameter.LATITUDE;
import static me.thisnow.service.util.Parameter.LONGITUDE;

public final class Payload {
    private static final Payload INSTANCE = new Payload();
    private Bundle data = null;

    private Payload() {

    }

    public static Payload getInstance() {
        return INSTANCE;
    }

    public Bundle getForAuth(Context context, String login, String password) {
        return getPayload(new Auth(context, login, password));
    }

    public Bundle getForNoAuth(Context context) {
        return getPayload(new NoAuth(context));
    }

    public Bundle getForLogout(Context context) {
        return getPayload(new Logout(context));
    }

    public Bundle getForRegistration(Context context, String login, String password, String name, String lastName, String address, int type, int subtype, String... keywords) {
        return getPayload(new Registration(context, login, password, name, lastName, address, type, subtype, keywords));
    }

    public Bundle getForAttachment(String url, String mediaType) {
        return getPayload(new Attachment(url, mediaType));
    }

    public Bundle getForSync(String token, double latitude, double longitude) {
        return getPayload(new Sync(token, latitude, longitude));
    }

    public Bundle getForObjectsList(int objectType, int radius, long offset, long limit, double latitude, double longitude) {
        return getPayload(new ObjectsList(objectType, radius, offset, limit, latitude, longitude));
    }

    private Bundle getPayload(ParametersBuilder parametersBuilder) {
        initData();
        parametersBuilder.putData(data);
        return data;
    }

    private void initData() {
        if (data == null) {
            data = new Bundle();
        } else {
            data.clear();
        }
    }

    public void putLocation(Bundle data, double latitude, double longitude) {
        data.putDouble(LATITUDE, latitude);
        data.putDouble(LONGITUDE, longitude);
    }

}