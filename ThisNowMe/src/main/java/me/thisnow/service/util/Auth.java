package me.thisnow.service.util;

import android.content.Context;
import android.os.Bundle;

import me.thisnow.util.Device;

import static me.thisnow.service.util.Parameter.IMEI;
import static me.thisnow.service.util.Parameter.LOGIN;
import static me.thisnow.service.util.Parameter.PASSWORD;

final class Auth extends ParametersBuilder {
    private final Context context;
    private final String login;
    private final String password;

    Auth(Context context, String login, String password) {
        this.context = context;
        this.login = login;
        this.password = password;
    }

    @Override
    public Bundle putData(Bundle data) {
        data.putString(IMEI, Device.pseudoUniqueID(context));
        data.putString(LOGIN, login);
        data.putString(PASSWORD, password);
        return data;
    }

}