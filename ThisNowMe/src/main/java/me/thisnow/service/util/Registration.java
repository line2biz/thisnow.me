package me.thisnow.service.util;

import android.content.Context;
import android.os.Bundle;

import me.thisnow.util.Device;

import static me.thisnow.service.util.Parameter.ADDRESS;
import static me.thisnow.service.util.Parameter.IMEI;
import static me.thisnow.service.util.Parameter.KEYWORDS;
import static me.thisnow.service.util.Parameter.LAST_NAME;
import static me.thisnow.service.util.Parameter.LOGIN;
import static me.thisnow.service.util.Parameter.NAME;
import static me.thisnow.service.util.Parameter.PASSWORD;
import static me.thisnow.service.util.Parameter.SUBTYPE;
import static me.thisnow.service.util.Parameter.TYPE;

final class Registration extends ParametersBuilder {
    private final Context context;
    private final String login;
    private final String password;
    private final String name;
    private final String lastName;
    private final String address;
    private final int type;
    private final int subtype;
    private final String[] keywords;

    Registration(Context context, String login, String password, String name, String lastName, String address, int type, int subtype, String... keywords) {
        this.context = context;
        this.login = login;
        this.password = password;
        this.name = name;
        this.lastName = lastName;
        this.address = address;
        this.type = type;
        this.subtype = subtype;
        this.keywords = keywords;
    }

    @Override
    public Bundle putData(Bundle data) {
        data.putString(IMEI, Device.pseudoUniqueID(context));
        data.putString(LOGIN, login);
        data.putString(PASSWORD, password);
        data.putString(NAME, name);
        putValue(data, LAST_NAME, lastName);
        data.putString(ADDRESS, address);
        data.putInt(TYPE, type);
        data.putInt(SUBTYPE, subtype);
        putValue(data, KEYWORDS, keywords);
        return data;
    }

}
