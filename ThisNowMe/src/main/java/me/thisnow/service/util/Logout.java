package me.thisnow.service.util;

import android.content.Context;
import android.os.Bundle;

import me.thisnow.singleton.AccountInfoHolder;
import me.thisnow.util.Device;

import static me.thisnow.service.util.Parameter.IMEI;
import static me.thisnow.service.util.Parameter.TOKEN;

final class Logout extends ParametersBuilder {
    private final Context context;

    Logout(Context context) {
        this.context = context;
    }

    @Override
    public Bundle putData(Bundle data) {
        data.putString(IMEI, Device.pseudoUniqueID(context));
        data.putString(TOKEN, AccountInfoHolder.getInstance().getToken());
        return data;
    }

}
