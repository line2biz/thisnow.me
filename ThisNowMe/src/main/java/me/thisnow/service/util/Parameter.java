package me.thisnow.service.util;

interface Parameter {
    String IMEI = "IMEI";
    String TOKEN = "token";
    String TYPE = "type";
    String SUBTYPE = "subtype";
    String LOGIN = "login";
    String PASSWORD = "password";
    String NAME = "name_1";
    String LAST_NAME = "name_2";
    String ADDRESS = "address";
    String LONGITUDE = "long";
    String LATITUDE = "lat";
    String OBJECT_ID = "object_id";
    String OBJECT_TYPE = "object_type";
    String RADIUS = "radius";
    String OFFSET = "offset";
    String LIMIT = "limit";
    String URL = "url";
    String MEDIA_TYPE = "media_type";
    String KEYWORDS = "keywords[]";
}