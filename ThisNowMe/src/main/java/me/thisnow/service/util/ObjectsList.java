package me.thisnow.service.util;

import android.os.Bundle;

import me.thisnow.singleton.AccountInfoHolder;

import static me.thisnow.service.util.Parameter.LATITUDE;
import static me.thisnow.service.util.Parameter.LIMIT;
import static me.thisnow.service.util.Parameter.LONGITUDE;
import static me.thisnow.service.util.Parameter.OBJECT_TYPE;
import static me.thisnow.service.util.Parameter.OFFSET;
import static me.thisnow.service.util.Parameter.RADIUS;
import static me.thisnow.service.util.Parameter.TOKEN;

final class ObjectsList extends ParametersBuilder {
    private final int objectType;
    private final int radius;
    private final long offset;
    private final long limit;
    private final double latitude;
    private final double longitude;


    ObjectsList(int objectType, int radius, long offset, long limit, double latitude, double longitude) {
        this.objectType = objectType;
        this.radius = radius;
        this.offset = offset;
        this.limit = limit;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public Bundle putData(Bundle data) {
        data.putString(TOKEN, AccountInfoHolder.getInstance().getToken());
        data.putDouble(LATITUDE, latitude);
        data.putDouble(LONGITUDE, longitude);
        data.putInt(OBJECT_TYPE, objectType);
        putValue(data, RADIUS, radius);
        putValue(data, OFFSET, offset);
        putValue(data, LIMIT, limit);
        return data;
    }

}