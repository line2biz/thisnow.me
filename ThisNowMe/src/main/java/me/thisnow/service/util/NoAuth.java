package me.thisnow.service.util;

import android.content.Context;
import android.os.Bundle;

import me.thisnow.util.Device;

import static me.thisnow.service.util.Parameter.IMEI;

final class NoAuth extends ParametersBuilder {
    private final Context context;

    NoAuth(Context context) {
        this.context = context;
    }

    @Override
    public Bundle putData(Bundle data) {
        data.putString(IMEI, Device.pseudoUniqueID(context));
        return null;
    }

}