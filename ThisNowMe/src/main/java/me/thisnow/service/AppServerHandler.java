package me.thisnow.service;

import android.content.Context;
import android.os.Bundle;

import com.google.gson.ExclusionStrategy;

import me.thisnow.R;
import me.thisnow.service.constant.Mode;
import me.thisnow.service.model.Response;
import me.thisnow.service.strategy.ObjectsList;
import me.thisnow.singleton.AccountInfoHolder;
import me.thisnow.singleton.ObjectHolder;
import me.thisnow.singleton.SubtypeHolder;
import me.thisnow.service.strategy.All;
import me.thisnow.service.strategy.Auth;
import me.thisnow.service.strategy.MediaSave;
import me.thisnow.service.strategy.NoAuth;
import me.thisnow.service.strategy.Subtypes;
import me.thisnow.util.Preferences;

import static me.thisnow.service.constant.Action.GET_AROUND_ME_BUSINESS_OBJECTS;
import static me.thisnow.service.constant.Action.GET_BUSINESS_SUBTYPES;
import static me.thisnow.service.constant.Action.GET_AROUND_ME_EVENT_OBJECTS;
import static me.thisnow.service.constant.Action.GET_EVENT_SUBTYPES;
import static me.thisnow.service.constant.Action.GET_AROUND_ME_PERSON_OBJECTS;
import static me.thisnow.service.constant.Action.GET_PERSON_SUBTYPES;
import static me.thisnow.service.constant.Action.GET_TIMELINE_BUSINESS_OBJECTS;
import static me.thisnow.service.constant.Action.GET_TIMELINE_EVENT_OBJECTS;
import static me.thisnow.service.constant.Action.GET_TIMELINE_PERSON_OBJECTS;
import static me.thisnow.service.constant.Action.LOGIN_AUTH;
import static me.thisnow.service.constant.Action.LOGIN_NO_AUTH;
import static me.thisnow.service.constant.Action.MEDIA_UPLOAD;
import static me.thisnow.service.constant.Action.REGISTER_BUSINESS;
import static me.thisnow.service.constant.Action.REGISTER_PERSON;
import static me.thisnow.service.constant.Authority.THIS_NOW_ME;
import static me.thisnow.service.constant.Method.GET;
import static me.thisnow.service.constant.Method.POST;
import static me.thisnow.service.constant.Mode.AROUND_ME;
import static me.thisnow.service.constant.Mode.TIMELINE;
import static me.thisnow.service.constant.Protocol.HTTP;
import static me.thisnow.service.ResponseCode.SERVER_CONNECTION_ERROR;
import static me.thisnow.service.constant.Type.BUSINESS;
import static me.thisnow.service.constant.Type.EVENT;
import static me.thisnow.service.constant.Type.PERSON;

public final class AppServerHandler extends AbstractServerHandler<Response> {

    public static void get(Context context, String action, String path) {
        request(context, AppServerHandler.class, action, GET, path, null);
    }

    public static void post(Context context, String action, String path, Bundle payload) {
        request(context, AppServerHandler.class, action, POST, path, payload);
    }

    @Override
    protected Response buildErrorResponse() {
        Response response = new Response();
        response.setCode(SERVER_CONNECTION_ERROR);
        response.setMessage(getResources().getString(R.string.error_server_problem));
        return response;
    }

    @Override
    protected String getProtocol() {
        return HTTP;
    }

    @Override
    protected String getHost() {
        return THIS_NOW_ME;
    }

    @Override
    protected void handleResponse(Response response, String action) {
        if (response.getData() != null) {
            if (GET_PERSON_SUBTYPES.equals(action)) {
                SubtypeHolder.getInstance().setSubtypes(PERSON, response.getData().getSubtypes());
            } else if (GET_BUSINESS_SUBTYPES.equals(action)) {
                SubtypeHolder.getInstance().setSubtypes(BUSINESS, response.getData().getSubtypes());
            } else if (GET_EVENT_SUBTYPES.equals(action)) {
                SubtypeHolder.getInstance().setSubtypes(EVENT, response.getData().getSubtypes());
            } else if (REGISTER_PERSON.equals(action)
                    || REGISTER_BUSINESS.equals(action)
                    || LOGIN_AUTH.equals(action)) {
                Preferences.setAccountToken(getApplicationContext(), response.getData().getToken());
                AccountInfoHolder.getInstance().setToken(response.getData().getToken());
                AccountInfoHolder.getInstance().setObjectType(response.getData().getObjectType());
                AccountInfoHolder.getInstance().setObjectId(response.getData().getObjectId());
            } else if (LOGIN_NO_AUTH.equals(action)) {
                Preferences.setAccountToken(getApplicationContext(), response.getData().getToken());
                AccountInfoHolder.getInstance().setToken(response.getData().getToken());
            } else if (GET_TIMELINE_PERSON_OBJECTS.equals(action)) {
                ObjectHolder.getInstance().addObjects(response.getData().getObjectsList(), TIMELINE, PERSON);
            } else if (GET_TIMELINE_BUSINESS_OBJECTS.equals(action)) {
                ObjectHolder.getInstance().addObjects(response.getData().getObjectsList(), TIMELINE, BUSINESS);
            } else if (GET_TIMELINE_EVENT_OBJECTS.equals(action)) {
                ObjectHolder.getInstance().addObjects(response.getData().getObjectsList(), TIMELINE, EVENT);
            } else if (GET_AROUND_ME_PERSON_OBJECTS.equals(action)) {
                ObjectHolder.getInstance().addObjects(response.getData().getObjectsList(), AROUND_ME, PERSON);
            } else if (GET_AROUND_ME_BUSINESS_OBJECTS.equals(action)) {
                ObjectHolder.getInstance().addObjects(response.getData().getObjectsList(), AROUND_ME, BUSINESS);
            } else if (GET_AROUND_ME_EVENT_OBJECTS.equals(action)) {
                ObjectHolder.getInstance().addObjects(response.getData().getObjectsList(), AROUND_ME, EVENT);
            }
        }
    }

    @Override
    protected ExclusionStrategy getExclusionStrategy(String action) {
        if (GET_PERSON_SUBTYPES.equals(action)
                || GET_BUSINESS_SUBTYPES.equals(action)
                || GET_EVENT_SUBTYPES.equals(action)) {
            return new Subtypes();
        } else if (REGISTER_PERSON.equals(action)
                || REGISTER_BUSINESS.equals(action)
                || LOGIN_AUTH.equals(action)) {
            return new Auth();
        } else if (LOGIN_NO_AUTH.equals(action)) {
            return new NoAuth();
        } else if (MEDIA_UPLOAD.equals(action)) {
            return new MediaSave();
        } else if (GET_TIMELINE_PERSON_OBJECTS.equals(action)
                || GET_TIMELINE_BUSINESS_OBJECTS.equals(action)
                || GET_TIMELINE_EVENT_OBJECTS.equals(action)
                || GET_AROUND_ME_PERSON_OBJECTS.equals(action)
                || GET_AROUND_ME_BUSINESS_OBJECTS.equals(action)
                || GET_AROUND_ME_EVENT_OBJECTS.equals(action)) {
            return new ObjectsList();
        } else {
            return new All();
        }
    }

    @Override
    protected Class<Response> getCls() {
        return Response.class;
    }

    @Override
    protected int getCode(Response response) {
        return response.getCode();
    }

    @Override
    protected String getMessage(Response response) {
        return response.getMessage();
    }

}