package me.thisnow.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import static me.thisnow.service.constant.Action.RESPONSE;
import static me.thisnow.service.Extra.ACTION;
import static me.thisnow.service.Extra.CODE;
import static me.thisnow.service.Extra.MESSAGE;
import static me.thisnow.service.ResponseCode.OK;
import static me.thisnow.service.ResponseCode.SERVER_CONNECTION_ERROR;

public final class ServerCallback extends BroadcastReceiver {
    private static final String TAG = ServerCallback.class.getSimpleName();
    private IntentFilter intentFilter = null;
    private EventListener eventListener = null;

    public ServerCallback(EventListener eventListener) {
        this.eventListener = eventListener;
        this.intentFilter = new IntentFilter();
        initActions();
    }

    private void initActions() {
        intentFilter.addAction(RESPONSE);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (eventListener != null) {
            if (RESPONSE.equals(intent.getAction())) {
                if (intent.getExtras() != null) {
                    String action = intent.getStringExtra(ACTION);
                    String message = intent.getStringExtra(MESSAGE);
                    int errorCode = intent.getIntExtra(CODE, SERVER_CONNECTION_ERROR);
                    if (errorCode == OK) {
                        eventListener.onSuccess(action, message);
                    } else {
                        eventListener.onError(action, message);
                    }
                }
            }
        }
    }

    public IntentFilter getFilter() {
        return intentFilter;
    }

    public interface EventListener {
        void onSuccess(String action, String message);

        void onError(String action, String message);
    }

}