package me.thisnow.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.format.DateUtils;
import android.util.Log;

import com.google.gson.ExclusionStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.squareup.okhttp.OkHttpClient;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static java.net.HttpURLConnection.HTTP_OK;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static me.thisnow.constant.Char.AND;
import static me.thisnow.constant.Char.EQUAL;
import static me.thisnow.service.Extra.ACTION;
import static me.thisnow.service.Extra.CODE;
import static me.thisnow.service.Extra.MESSAGE;
import static me.thisnow.service.Extra.METHOD;
import static me.thisnow.service.Extra.PATH;
import static me.thisnow.service.Extra.PAYLOAD;
import static me.thisnow.service.constant.Method.POST;
import static me.thisnow.service.constant.Action.RESPONSE;

abstract class AbstractServerHandler<T> extends IntentService {
    protected static final String TAG = AbstractServerHandler.class.getSimpleName();
    protected static final String UTF_8 = "UTF-8";
    private static final long CONNECTION_TIMEOUT = 5 * DateUtils.SECOND_IN_MILLIS;
    private static final long READ_TIMEOUT = 15 * DateUtils.SECOND_IN_MILLIS;
    private final OkHttpClient httpClient;

    public AbstractServerHandler() {
        this(TAG);
    }

    public AbstractServerHandler(String name) {
        super(name);
        httpClient = new OkHttpClient();
        httpClient.setConnectTimeout(CONNECTION_TIMEOUT, MILLISECONDS);
        httpClient.setReadTimeout(READ_TIMEOUT, MILLISECONDS);
    }

    protected static void request(Context context, Class cls, String action, String method, String path, Bundle payload) {
        Intent intent = new Intent(context, cls);
        intent.setAction(action);
        intent.putExtra(METHOD, method);
        intent.putExtra(PATH, path);
        intent.putExtra(PAYLOAD, payload);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            Log.d(TAG, "intent received!");
            if (intent.getAction() != null) {
                final String action = intent.getAction();
                final String method = intent.getStringExtra(METHOD);
                final String path = intent.getStringExtra(PATH);
                final byte[] payload = (POST.equals(method)) ? getPayloadBytes(intent.getBundleExtra(PAYLOAD)) : null;
                Log.d(TAG, "intent extra: " + action + ", " + path);
                T response = buildErrorResponse();
                OutputStream output = null;
                InputStream input = null;
                try {
                    URL url = getUrl(path);
                    HttpURLConnection connection = openConnection(url);
                    connection.setRequestMethod(method);
                    if (payload != null) {
                        connection.setDoOutput(true);
                        output = connection.getOutputStream();
                        sendPayload(output, payload);
                    }
                    checkResponseCode(connection);
                    input = connection.getInputStream();
                    response = getResponse(action, input);
                    Log.d(TAG, "response received!");
                    handleResponse(response, action);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                } finally {
                    close(output);
                    close(input);
                }
                Log.d(TAG, "send broadcast!");
                sendLocalBroadcast(getCode(response), getMessage(response), action);
            }
        }
    }

    private byte[] getPayloadBytes(Bundle bundle) {
        try {
            StringBuilder builder = new StringBuilder();
            for (String key : bundle.keySet()) {
                if (builder.length() != 0) {
                    builder.append(AND);
                }
                builder.append(key).append(EQUAL).append(bundle.get(key));
            }
            Log.d(TAG, "request params: " + builder.toString());
            return builder.toString().getBytes(UTF_8);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    private URL getUrl(String path) throws MalformedURLException, UnsupportedEncodingException {
        return new URL(getProtocol(), getHost(), path);
    }

    private HttpURLConnection openConnection(URL url) {
        return httpClient.open(url);
    }

    private void sendPayload(OutputStream output, byte[] payload) throws IOException {
        output.write(payload);
        output.flush();
        output.close();
    }

    private void checkResponseCode(HttpURLConnection connection) throws IOException {
        if (connection.getResponseCode() != HTTP_OK) {
            throw new IOException("Unexpected HTTP response: "
                    + connection.getResponseCode() + " " + connection.getResponseMessage());
        }
    }

    private T getResponse(String action, InputStream input) throws IOException {
        return buildJsonHandler(action).fromJson(new InputStreamReader(input), getCls());
    }

    private Gson buildJsonHandler(String action) {
        return new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
                .setExclusionStrategies(getExclusionStrategy(action)).create();
    }

    private void close(Closeable stream) {
        try {
            if (stream != null) {
                stream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendLocalBroadcast(int code, String message, String action) {
        Intent intent = new Intent(RESPONSE);
        intent.putExtra(ACTION, action);
        intent.putExtra(CODE, code);
        intent.putExtra(MESSAGE, message);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    protected abstract T buildErrorResponse();

    protected abstract String getProtocol();

    protected abstract String getHost();

    protected abstract void handleResponse(T response, String action);

    protected abstract ExclusionStrategy getExclusionStrategy(String action);

    protected abstract Class<T> getCls();

    protected abstract int getCode(T response);

    protected abstract String getMessage(T response);

}