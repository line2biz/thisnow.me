package me.thisnow.service;

interface Extra {
    String EXTRA_BASE = Extra.class.getCanonicalName();
    String METHOD = EXTRA_BASE + ".Method";
    String PATH = EXTRA_BASE + ".Path";
    String PAYLOAD = EXTRA_BASE + ".Payload";
    String ACTION = EXTRA_BASE + ".Action";
    String MESSAGE = EXTRA_BASE + ".Message";
    String CODE = EXTRA_BASE + ".Code";
    String FILE_PATH = EXTRA_BASE + ".FilePath";
    String MEDIA_TYPE= EXTRA_BASE + ".MediaType";
}