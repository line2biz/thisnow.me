package me.thisnow.database.model;

import android.content.ContentValues;
import android.database.Cursor;

abstract class DatabaseEntry {

    public abstract void getEntryFromCursor(Cursor cursor);

    public abstract ContentValues getContentValues();

}