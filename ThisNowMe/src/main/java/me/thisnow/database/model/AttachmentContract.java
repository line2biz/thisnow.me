package me.thisnow.database.model;

import android.net.Uri;

import static android.provider.BaseColumns._ID;
import static me.thisnow.database.DatabaseProvider.AUTHORITY_CONTENT;
import static me.thisnow.database.DatabaseProvider.BASE_CONTENT_URI;

interface AttachmentContract {
    String TOKEN = "token";
    String OBJECT_ID = "object_id";
    String PATH = "path";
    String MEDIA_TYPE = "media_type";

    String TABLE_NAME = "attachments_db_table";
    String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + AUTHORITY_CONTENT.replace("com", "vnd") + "." + TABLE_NAME;
    String CONTENT_TYPE = "vnd.android.cursor.dir/" + AUTHORITY_CONTENT.replace("com", "vnd") + "." + TABLE_NAME;
    String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
            + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + TOKEN + " TEXT NOT NULL, "
            + OBJECT_ID + " TEXT NOT NULL, "
            + PATH + " TEXT NOT NULL, "
            + MEDIA_TYPE + " TEXT NOT NULL);";
    Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();
}