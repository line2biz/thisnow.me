package me.thisnow.database.model;

import android.content.ContentValues;
import android.database.Cursor;

import static android.provider.BaseColumns._ID;

public final class Attachment extends DatabaseEntry implements AttachmentContract {
    private String token;
    private String objectId;
    private String path;
    private String mediaType;
    private long id;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public void getEntryFromCursor(Cursor cursor) {
        token = cursor.getString(cursor.getColumnIndex(TOKEN));
        objectId = cursor.getString(cursor.getColumnIndex(OBJECT_ID));
        path = cursor.getString(cursor.getColumnIndex(PATH));
        mediaType = cursor.getString(cursor.getColumnIndex(MEDIA_TYPE));
        id = cursor.getLong(cursor.getColumnIndex(_ID));
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(TOKEN, token);
        values.put(OBJECT_ID, objectId);
        values.put(PATH, path);
        values.put(MEDIA_TYPE, mediaType);
        return values;
    }
}