package me.thisnow.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static me.thisnow.database.model.Attachment.CREATE_TABLE;
import static me.thisnow.database.model.Attachment.DROP_TABLE;

public final class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "This_Now_Me_Database";
    private static final String TAG = DatabaseHelper.class.getSimpleName();
    private static final int DATABASE_VERSION = 1;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.w(TAG, "database create - version " + DATABASE_VERSION);
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        Log.w(TAG, "database upgrade - version from " + oldVersion + " to " + newVersion);
        sqLiteDatabase.execSQL(DROP_TABLE);
        onCreate(sqLiteDatabase);
    }

}