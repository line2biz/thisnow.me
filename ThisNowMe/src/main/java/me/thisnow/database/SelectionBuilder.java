package me.thisnow.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/*
 * Copyright (C) 2010 The Android Open Source Project Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a copy of the
 * License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in
 * writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

/*
 * Modifications: -Imported from AOSP frameworks/base/core/java/com/android/internal/content -Changed package
 * name
 */
public final class SelectionBuilder {
    private static final String TAG = SelectionBuilder.class.getSimpleName();
    private static final boolean LOGV = false;
    private final Map<String, String> mProjectionMap = new HashMap<String, String>();
    private final StringBuilder mSelection = new StringBuilder();
    private final ArrayList<String> mSelectionArgs = new ArrayList<String>();
    private String mTable = null;

    private void assertTable() {
        if (mTable == null) {
            throw new IllegalStateException("Table not specified");
        }
    }

    public int delete(SQLiteDatabase db) {
        assertTable();
        if (LOGV) {
            Log.v(TAG, "delete() " + this);
        }
        return db.delete(mTable, getSelection(), getSelectionArgs());
    }

    public String getSelection() {
        return mSelection.toString();
    }

    public String[] getSelectionArgs() {
        return mSelectionArgs.toArray(new String[mSelectionArgs.size()]);
    }

    public SelectionBuilder map(String fromColumn, String toClause) {
        mProjectionMap.put(fromColumn, toClause + " AS " + fromColumn);
        return this;
    }

    private void mapColumns(String[] columns) {
        for (int i = 0; i < columns.length; i++) {
            final String target = mProjectionMap.get(columns[i]);
            if (target != null) {
                columns[i] = target;
            }
        }
    }

    public SelectionBuilder mapToTable(String column, String table) {
        mProjectionMap.put(column, table + "." + column);
        return this;
    }

    public Cursor query(SQLiteDatabase db, String[] columns, String orderBy) {
        return query(db, columns, null, null, orderBy, null);
    }

    public Cursor query(SQLiteDatabase db, String[] columns, String groupBy, String having, String orderBy, String limit) {
        assertTable();
        if (columns != null) {
            mapColumns(columns);
        }
        if (LOGV) {
            Log.v(TAG, "query(columns=" + Arrays.toString(columns) + ") " + this);
        }
        return db.query(mTable, columns, getSelection(), getSelectionArgs(), groupBy, having, orderBy, limit);
    }

    public SelectionBuilder reset() {
        mTable = null;
        mSelection.setLength(0);
        mSelectionArgs.clear();
        return this;
    }

    public SelectionBuilder table(String table) {
        mTable = table;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(TAG);
        builder.append("[table=").append(mTable).append(", ");
        builder.append("selection=").append(getSelection()).append(", ");
        builder.append("selectionArgs=").append(Arrays.toString(getSelectionArgs())).append("]");
        return builder.toString();
    }

    public int update(SQLiteDatabase db, ContentValues values) {
        assertTable();
        if (LOGV) {
            Log.v(TAG, "update() " + this);
        }
        return db.update(mTable, values, getSelection(), getSelectionArgs());
    }

    public SelectionBuilder where(String selection, String... selectionArgs) {
        if (TextUtils.isEmpty(selection)) {
            if ((selectionArgs != null) && (selectionArgs.length > 0)) {
                throw new IllegalArgumentException("Valid selection required when including arguments=");
            }
            return this;
        }
        if (mSelection.length() > 0) {
            mSelection.append(" AND ");
        }
        mSelection.append("(").append(selection).append(")");
        if (selectionArgs != null) {
            Collections.addAll(mSelectionArgs, selectionArgs);
        }
        return this;
    }

}