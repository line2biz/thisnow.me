package me.thisnow.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;
import android.util.Log;

import me.thisnow.database.model.Attachment;

import static android.content.UriMatcher.NO_MATCH;

public final class DatabaseProvider extends ContentProvider {
    private static final String TAG = DatabaseProvider.class.getSimpleName();
    public static final String AUTHORITY_CONTENT = "me.thisnow.database";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY_CONTENT);
    private static final String ITEM_POINTER = "/#";
    private static final int ATTACHMENT_DIR_ID = 0x0111;
    private static final int ATTACHMENT_ITEM_ID = 0x0112;
    private static final UriMatcher URI_MATCHER = buildUriMatcher();
    private DatabaseHelper mDatabaseHelper;

    @Override
    public boolean onCreate() {
        mDatabaseHelper = new DatabaseHelper(getContext());
        return (mDatabaseHelper != null);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase database = getInstance();
        final Cursor cursor = getSimpleSelectionBuilder(uri, selection, selectionArgs).query(database, projection, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case ATTACHMENT_DIR_ID:
                return Attachment.CONTENT_TYPE;
            case ATTACHMENT_ITEM_ID:
                return Attachment.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("unknown Uri: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase database = getInstance();
        Uri insertedUri;
        long insertedId;
        switch (URI_MATCHER.match(uri)) {
            case ATTACHMENT_DIR_ID:
                insertedId = database.insertOrThrow(Attachment.TABLE_NAME, null, values);
                insertedUri = ContentUris.withAppendedId(Attachment.CONTENT_URI, insertedId);
                break;
            default:
                throw new IllegalArgumentException("unknown Uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(insertedUri, null);
        return insertedUri;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        switch (URI_MATCHER.match(uri)) {
            case ATTACHMENT_DIR_ID:
                if (TextUtils.isEmpty(selection)) {
                    throw new IllegalArgumentException("specify selection for location dir update: " + uri);
                }
                break;
            case ATTACHMENT_ITEM_ID:
                break;
            default:
                throw new IllegalArgumentException("unknown uri: " + uri);
        }
        int count = insertOrUpdate(uri, values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase database = getInstance();
        int count = getSimpleSelectionBuilder(uri, selection, selectionArgs).delete(database);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    private SQLiteDatabase getInstance() {
        SQLiteDatabase database;
        try {
            database = mDatabaseHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            Log.w(TAG, "open database error: " + e.toString());
            mDatabaseHelper = new DatabaseHelper(getContext());
            database = mDatabaseHelper.getWritableDatabase();
        }
        return database;
    }

    private int insertOrUpdate(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
        int count = getSimpleSelectionBuilder(uri, selection, selectionArgs).update(getInstance(), contentValues);
        if (count == 0) {
            insert(uri, contentValues);
            return 1;
        }
        return count;
    }

    private SelectionBuilder getSimpleSelectionBuilder(Uri uri, String selection, String[] selectionArgs) {
        SelectionBuilder builder = new SelectionBuilder();
        String id;
        switch (URI_MATCHER.match(uri)) {
            case ATTACHMENT_DIR_ID:
                builder.table(Attachment.TABLE_NAME);
                if (TextUtils.isEmpty(selection)) {
                    builder.where(null, (String[]) null);
                } else {
                    builder.where(selection, selectionArgs);
                }
                break;
            case ATTACHMENT_ITEM_ID:
                id = uri.getPathSegments().get(1);
                builder.table(Attachment.TABLE_NAME).where(BaseColumns._ID + "=?", id);
                break;
            default:
                throw new IllegalArgumentException("unknown uri: " + uri);
        }
        return builder;
    }

    private static UriMatcher buildUriMatcher() {
        UriMatcher uriMatcher = new UriMatcher(NO_MATCH);

        uriMatcher.addURI(AUTHORITY_CONTENT, Attachment.TABLE_NAME, ATTACHMENT_DIR_ID);
        uriMatcher.addURI(AUTHORITY_CONTENT, Attachment.TABLE_NAME + ITEM_POINTER, ATTACHMENT_ITEM_ID);

        return uriMatcher;
    }

}