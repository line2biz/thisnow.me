package me.thisnow.activity;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import me.thisnow.R;
import me.thisnow.fragment.AppInfo;
import me.thisnow.fragment.ObjectGrid;
import me.thisnow.fragment.bundle.ObjectGridArgs;
import me.thisnow.fragment.bundle.Parameter;
import me.thisnow.fragment.bundle.RegistrationArgs;
import me.thisnow.service.AppServerHandler;
import me.thisnow.service.ServerCallback;
import me.thisnow.service.constant.Action;
import me.thisnow.service.constant.Mode;
import me.thisnow.service.constant.Path;
import me.thisnow.service.constant.Tab;
import me.thisnow.service.util.Payload;
import me.thisnow.singleton.SubtypeHolder;
import me.thisnow.util.FusedLocation;
import me.thisnow.util.Message;
import me.thisnow.util.Network;

import static android.view.View.GONE;
import static android.view.View.OnClickListener;
import static android.view.View.VISIBLE;
import static me.thisnow.service.ServerCallback.EventListener;
import static me.thisnow.service.constant.Action.REGISTER_EVENT;
import static me.thisnow.service.constant.Path.TYPE_BASE;
import static me.thisnow.service.constant.Type.EVENT;
import static me.thisnow.util.FusedLocation.CONNECTION_FAILURE_RESOLUTION_REQUEST;

public final class ObjectViewer extends TabsHolderBasic implements OnClickListener, EventListener {
    private static final String TAG = ObjectViewer.class.getSimpleName();
    private static final int TAB_TIMELINE = 0;
    private static final int TAB_AROUND_ME = 1;
    private static final int TAB_SETTINGS = 2;
    private static final int TAB_EVENTS = 3;
    private static final int TAB_CHAT = 4;
    private static final TabHolder[] TABS = new TabHolder[]{
            new TabHolder(ObjectGrid.class, Tab.Title.TIMELINE, Tab.Tag.TIMELINE, R.drawable.tab_timeline, R.color.app_vinous, false),
            new TabHolder(ObjectGrid.class, Tab.Title.AROUND_ME, Tab.Tag.AROUND_ME, R.drawable.tab_around_me, R.color.app_red, false),
            new TabHolder(AppInfo.class, Tab.Title.SETTINGS, Tab.Tag.SETTINGS, R.drawable.tab_settings, R.color.app_yellow, false),
            new TabHolder(AppInfo.class, Tab.Title.EVENTS, Tab.Tag.EVENTS, R.drawable.tab_events, R.color.app_green, true),
            new TabHolder(AppInfo.class, Tab.Title.CHAT, Tab.Tag.CHAT, R.drawable.tab_chat, R.color.app_violet, false)
    };
    private ServerCallback mServerCallback;
    private RelativeLayout mTabTitleHolder;
    private TextView mTitle;
    private ImageButton mAction;
    private ImageButton mLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews();
        initListeners();
        mServerCallback = new ServerCallback(this);
        FusedLocation.getInstance().startProviderService(getApplicationContext());
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mServerCallback, mServerCallback.getFilter());
        FusedLocation.getInstance().setActivity(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mServerCallback);
        FusedLocation.getInstance().setActivity(null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CONNECTION_FAILURE_RESOLUTION_REQUEST:
                FusedLocation.getInstance().handleResolutionResult(getApplicationContext(), resultCode);
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tab_title_action:
                handleAction(getCurrentTab());
                break;
            case R.id.ib_logout:
                AppServerHandler.post(getApplicationContext(), Action.LOGOUT, Path.LOGOUT, Payload.getInstance().getForLogout(getApplicationContext()));
                break;
        }
    }

    @Override
    public void onSuccess(String action, String message) {
        if (Action.LOGOUT.equals(action)) {
            FusedLocation.getInstance().stopProviderService(getApplicationContext());
            startActivity(new Intent(this, Launcher.class));
            finish();
        } else if (Action.GET_EVENT_SUBTYPES.equals(action)) {
            registerEvent();
        }
    }

    @Override
    public void onError(String action, String message) {
        Message.show(getApplicationContext(), message);
    }

    @Override
    protected void getEvent(int event, Bundle args) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_object_viewer;
    }

    @Override
    protected int getFragmentContainerId() {
        return android.R.id.tabcontent;
    }

    @Override
    protected int getTabsCount() {
        return TABS.length;
    }

    @Override
    protected String getTabTitle(int position) {
        return TABS[position].title;
    }

    @Override
    protected String getTabTag(int position) {
        return TABS[position].tag;
    }

    @Override
    protected View getTabIndicator(int position) {
        return getView(TABS[position].drawableId, TABS[position].colorId);
    }

    @Override
    protected Class getFragmentCls(int position) {
        return TABS[position].cls;
    }

    @Override
    protected Bundle getFragmentArgs(int position) {
        switch (position) {
            case TAB_TIMELINE:
                return new ObjectGridArgs(Mode.TIMELINE).getData();
            case TAB_AROUND_ME:
                return new ObjectGridArgs(Mode.AROUND_ME).getData();
            case TAB_SETTINGS:
                return null;
            case TAB_EVENTS:
                return null;
            case TAB_CHAT:
                return null;
        }
        return null;
    }

    @Override
    protected int getTabDrawableDivider() {
        return R.drawable.divider_vertical_line_dark_gray;
    }

    @Override
    protected int getTabSideStripDrawable() {
        return R.drawable.tabs_side_strip;
    }

    @Override
    protected void handleTabChangeEvent(int tabId) {
        setTabTitleBackgroundColor(TABS[tabId].colorId);
        mTitle.setText(TABS[tabId].title);
        mAction.setVisibility(TABS[tabId].showAction ? VISIBLE : GONE);
    }

    private void initViews() {
        mTabTitleHolder = (RelativeLayout) findViewById(R.id.tab_title_holder);
        mTitle = (TextView) findViewById(R.id.tab_title_text);
        mAction = (ImageButton) findViewById(R.id.tab_title_action);
        mLogout = (ImageButton) findViewById(R.id.ib_logout);
    }

    private void initListeners() {
        mAction.setOnClickListener(this);
        mLogout.setOnClickListener(this);
    }

    private View getView(int imageResourceId, int colorId) {
        ImageView imageView = (ImageView) LayoutInflater.from(getApplicationContext()).inflate(R.layout.tab_indicator, null);
        imageView.setImageResource(imageResourceId);
        imageView.setBackgroundColor(getResources().getColor(colorId));
        return imageView;
    }

    private void setTabTitleBackgroundColor(int colorId) {
        ((ColorDrawable) ((LayerDrawable) mTabTitleHolder.getBackground()).getDrawable(1)).setColor(getResources().getColor(colorId));
    }

    private void handleAction(int tabId) {
        switch (tabId) {
            case TAB_TIMELINE:
                break;
            case TAB_AROUND_ME:
                break;
            case TAB_SETTINGS:
                break;
            case TAB_EVENTS:
                if (SubtypeHolder.getInstance().getSubtypeCount(EVENT) > 0) {
                    registerEvent();
                } else {
                    if (Network.isConnected(getApplicationContext())) {
                        AppServerHandler.get(getApplicationContext(), Action.GET_EVENT_SUBTYPES, TYPE_BASE + EVENT);
                    } else {
                        Network.checkConnectionMessage(getApplicationContext());
                    }
                }
                break;
            case TAB_CHAT:
                break;
        }
    }

    private void registerEvent() {
        Intent eventRegistration = new Intent(getApplicationContext(), ObjectViewerExtra.class);
        eventRegistration.putExtra(Action.BASE, REGISTER_EVENT);
        eventRegistration.putExtra(Parameter.BASE, new RegistrationArgs(EVENT).getData());
        startActivity(eventRegistration);
    }

}