package me.thisnow.activity;

import android.app.Fragment;
import android.os.Bundle;

import me.thisnow.R;
import me.thisnow.fragment.ChooseAttachments;
import me.thisnow.fragment.Registration;
import me.thisnow.fragment.UploadAttachments;
import me.thisnow.fragment.bundle.Parameter;
import me.thisnow.service.constant.Action;

import static me.thisnow.constant.AppEvent.ATTACHMENT_UPLOAD__SUCCESS;
import static me.thisnow.constant.AppEvent.OPEN_UPLOAD_ATTACHMENTS;
import static me.thisnow.constant.AppEvent.REGISTRATION_SUCCESS;
import static me.thisnow.service.constant.Action.REGISTER_EVENT;

public final class ObjectViewerExtra extends Basic {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent() != null && getIntent().hasExtra(Action.BASE) && getIntent().hasExtra(Parameter.BASE)) {
            final String action = getIntent().getStringExtra(Action.BASE);
            final Bundle args = getIntent().getBundleExtra(Parameter.BASE);
            mFragmentHandler.initCommit(getInitFragment(action), args);
        } else {
            finish();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_basic_with_header;
    }

    @Override
    protected int getFragmentContainerId() {
        return R.id.container;
    }

    @Override
    protected void getEvent(int event, Bundle args) {
        switch (event) {
            case OPEN_UPLOAD_ATTACHMENTS:
                mFragmentHandler.commit(new UploadAttachments(), args, null);
                break;
            case REGISTRATION_SUCCESS:
                mFragmentHandler.commit(new ChooseAttachments(), null, null);
                break;
            case ATTACHMENT_UPLOAD__SUCCESS:
                finish();
                break;
        }
    }

    private Fragment getInitFragment(String action) {
        if (REGISTER_EVENT.equals(action)) {
            return new Registration();
        }
        return null;
    }

}