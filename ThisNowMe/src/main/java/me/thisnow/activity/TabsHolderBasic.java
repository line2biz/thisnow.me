package me.thisnow.activity;

import android.os.Bundle;
import android.support.v13.app.FragmentTabHost;
import android.view.View;
import android.widget.TabHost;

abstract class TabsHolderBasic extends Basic implements TabHost.OnTabChangeListener {
    private FragmentTabHost mTabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup(getApplicationContext(), getFragmentManager(), android.R.id.tabcontent);
        for (int i = 0; i < getTabsCount(); i++) {
            mTabHost.addTab(mTabHost.newTabSpec(getTabTag(i)).setIndicator(getTabIndicator(i)), getFragmentCls(i), getFragmentArgs(i));
        }
        mTabHost.getTabWidget().setDividerDrawable(getTabDrawableDivider());
        mTabHost.getTabWidget().setStripEnabled(true);
        mTabHost.getTabWidget().setLeftStripDrawable(getTabSideStripDrawable());
        mTabHost.getTabWidget().setRightStripDrawable(getTabSideStripDrawable());
        mTabHost.setOnTabChangedListener(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (savedInstanceState == null) {
            handleTabChangeEvent(0);
        }
    }

    @Override
    public void onTabChanged(String tabId) {
        handleTabChangeEvent(mTabHost.getCurrentTab());
    }

    protected int getCurrentTab() {
        return mTabHost.getCurrentTab();
    }

    protected abstract int getTabsCount();

    protected abstract String getTabTitle(int position);

    protected abstract String getTabTag(int position);

    protected abstract View getTabIndicator(int position);

    protected abstract Class getFragmentCls(int position);

    protected abstract Bundle getFragmentArgs(int position);

    protected abstract int getTabDrawableDivider();

    protected abstract int getTabSideStripDrawable();

    protected abstract void handleTabChangeEvent(int tabId);

    static class TabHolder {
        final Class cls;
        final String title;
        final String tag;
        final int drawableId;
        final int colorId;
        final boolean showAction;

        TabHolder(Class cls, String title, String tag, int drawableId, int colorId, boolean showAction) {
            this.cls = cls;
            this.title = title;
            this.tag = tag;
            this.drawableId = drawableId;
            this.colorId = colorId;
            this.showAction = showAction;
        }

    }

}
