package me.thisnow.activity;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import me.thisnow.R;
import me.thisnow.fragment.AppInfo;
import me.thisnow.fragment.ChooseAttachments;
import me.thisnow.fragment.ChooseAuthenticationType;
import me.thisnow.fragment.Login;
import me.thisnow.fragment.Registration;
import me.thisnow.fragment.UploadAttachments;
import me.thisnow.singleton.UploadAttachmentsHolder;
import me.thisnow.util.FusedLocation;
import me.thisnow.util.Preferences;

import static me.thisnow.constant.AppEvent.ATTACHMENT_UPLOAD__SUCCESS;
import static me.thisnow.constant.AppEvent.LOGIN_SUCCESS;
import static me.thisnow.constant.AppEvent.OPEN_AUTHENTICATION;
import static me.thisnow.constant.AppEvent.OPEN_LOGIN;
import static me.thisnow.constant.AppEvent.OPEN_REGISTRATION;
import static me.thisnow.constant.AppEvent.OPEN_UPLOAD_ATTACHMENTS;
import static me.thisnow.constant.AppEvent.REGISTRATION_SUCCESS;
import static me.thisnow.fragment.UploadAttachments.REGISTRATION;
import static me.thisnow.fragment.bundle.Parameter.UPLOAD_TYPE;

public final class Launcher extends Basic {
    private static final String TAG = Launcher.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            FusedLocation.getInstance().stopProviderService(getApplicationContext());
            mFragmentHandler.initCommit(getInitFragment(), null);
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = mFragmentHandler.getCurrent();
        if (fragment instanceof UploadAttachments
                && fragment.getArguments() != null
                && fragment.getArguments().getInt(UPLOAD_TYPE) == REGISTRATION) {
            Log.d(TAG, "attachments upload: stop upload and remove all attachments from DB");
            UploadAttachmentsHolder.getInstance().setHandlerStopState(true);
            UploadAttachmentsHolder.getInstance().deleteAll(getApplicationContext());
        }
        super.onBackPressed();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_basic_with_header;
    }

    @Override
    protected int getFragmentContainerId() {
        return R.id.container;
    }

    @Override
    protected void getEvent(int event, Bundle args) {
        switch (event) {
            case OPEN_AUTHENTICATION:
                mFragmentHandler.setPrimary(new ChooseAuthenticationType(), null);
                break;
            case OPEN_LOGIN:
                mFragmentHandler.commit(new Login(), null, null);
                break;
            case OPEN_REGISTRATION:
                mFragmentHandler.commit(new Registration(), args, null);
                break;
            case OPEN_UPLOAD_ATTACHMENTS:
                mFragmentHandler.commit(new UploadAttachments(), args, null);
                break;
            case REGISTRATION_SUCCESS:
                mFragmentHandler.commit(new ChooseAttachments(), args, null);
                break;
            case LOGIN_SUCCESS:
            case ATTACHMENT_UPLOAD__SUCCESS:
                startActivity(new Intent(this, ObjectViewer.class));
                finish();
                break;
        }
    }

    private Fragment getInitFragment() {
        return (Preferences.checkAppInfoShown(getApplicationContext())) ? new ChooseAuthenticationType() : new AppInfo();
    }

}