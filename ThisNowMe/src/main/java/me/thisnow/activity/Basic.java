package me.thisnow.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.bugsense.trace.BugSenseHandler;

import me.thisnow.R;
import me.thisnow.util.FragmentHandler;

import me.thisnow.fragment.OnInteractionListener;

abstract class Basic extends FragmentActivity implements OnInteractionListener {
    protected FragmentHandler mFragmentHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BugSenseHandler.initAndStartSession(getApplicationContext(), getString(R.string.bug_sense_api_key));
        setContentView(getLayoutId());
        getWindow().setBackgroundDrawable(null);
        mFragmentHandler = new FragmentHandler(getFragmentManager(), getFragmentContainerId());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BugSenseHandler.closeSession(getApplicationContext());
    }

    @Override
    public void onEvent(int event, Bundle args) {
        getEvent(event, args);
    }

    protected abstract int getLayoutId();

    protected abstract int getFragmentContainerId();

    protected abstract void getEvent(int event, Bundle args);

}