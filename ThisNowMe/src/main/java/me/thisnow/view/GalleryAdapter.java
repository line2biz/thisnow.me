package me.thisnow.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.makeramen.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.imageaware.ImageViewAware;

import java.util.ArrayList;

import me.thisnow.R;
import me.thisnow.singleton.ObjectHolder;
import me.thisnow.singleton.model.ObjectItem;
import me.thisnow.util.Graphics;

import static me.thisnow.service.constant.Type.ABSENT;

final class GalleryAdapter extends PagerAdapter implements ImageLoadingListener {
    private static final String TAG = GalleryAdapter.class.getSimpleName();
    private final LayoutInflater mInflater;
    private final ArrayList<ObjectItem> mObjectItems;
    private final int mDisplayWidth;
    private final int mCornerRadius;
    private int mColor;

    public GalleryAdapter(Context context, int mode, int type) {
        this.mInflater = LayoutInflater.from(context);
        this.mObjectItems = ObjectHolder.getInstance().getList(mode, type);
        this.mDisplayWidth = context.getResources().getDisplayMetrics().widthPixels;
        this.mCornerRadius = mDisplayWidth / 2;
        this.mColor = ObjectHolder.getInstance().getBorderColor(context, ABSENT);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        RoundedImageView objectView = (RoundedImageView) mInflater.inflate(R.layout.object_detail_pager_item, null);
        ImageLoader.getInstance().displayImage(mObjectItems.get(position).getImage(), new ImageViewAware(objectView, false), this);
        objectView.setCornerRadius(mCornerRadius);
        objectView.setBorderColor(mColor);
        container.addView(objectView);
        Log.d(TAG, "item: instantiate " + position + ", index " + container.indexOfChild(objectView));
        return objectView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        int index = container.indexOfChild((View) object);
        container.removeViewAt(index);
        Log.d(TAG, "item: destroy " + position + ", index " + index);
    }

    @Override
    public int getCount() {
        return mObjectItems.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    @Override
    public void onLoadingStarted(String imageUri, View view) {

    }

    @Override
    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
        showBitmap((RoundedImageView) view, null);
    }

    @Override
    public void onLoadingCancelled(String imageUri, View view) {
        showBitmap((RoundedImageView) view, null);
    }

    @Override
    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
        showBitmap((RoundedImageView) view, loadedImage);
    }

    private void showBitmap (RoundedImageView imageView, Bitmap image) {
        imageView.setImageBitmap(Graphics.getInstance().getSquareBitmapScaledToWidth(image, mDisplayWidth));
    }

}