package me.thisnow.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import static android.support.v4.view.ViewPager.PageTransformer;

public final class Gallery extends ViewPager implements PageTransformer {
    private static final String TAG = Gallery.class.getSimpleName();
    private static final int OFF_SCREEN_PAGE_LIMIT = 3;
    private static final float INIT_SCALE = 1.0f;
    private static final float OFFSET_THRESHOLD = INIT_SCALE - GalleryOptions.MARGIN_SCALE;

    public Gallery(Context context) {
        super(context);
        init();
    }

    public Gallery(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setOffscreenPageLimit(OFF_SCREEN_PAGE_LIMIT);
        setChildrenDrawingOrderEnabled(true);
        setPageTransformer(false, this);
        setPageMargin(-getItemMargin());
    }

    private int getItemMargin() {
        return (int) (getResources().getDisplayMetrics().widthPixels * GalleryOptions.MARGIN_SCALE);
    }

    @Override
    protected int getChildDrawingOrder(int childCount, int i) {
//        Log.d(TAG, "drawing order: child " + i);
        return i;
    }

    @Override
    public void transformPage(View page, float positionOffset) {
//        Log.d(TAG, "page transform: index " + indexOfChild(page) + ", position offset " + positionOffset);
        float scale = getScale(Math.abs(positionOffset));
        page.setScaleX(scale);
        page.setScaleY(scale);
    }

    private float getScale(float absoluteOffset) {
        return ((absoluteOffset < OFFSET_THRESHOLD) ? (INIT_SCALE - absoluteOffset  / OFFSET_THRESHOLD) * GalleryOptions.DIFF_SCALE : 0) + GalleryOptions.MIN_SCALE;
    }

    public final void setObjects(int mode, int type) {
        setAdapter(new GalleryAdapter(getContext().getApplicationContext(), mode, type));
    }

}