package me.thisnow.view;

interface GalleryOptions {
    float MAX_SCALE = 0.9f;
    float MIN_SCALE = 0.3f;
    float DIFF_SCALE = MAX_SCALE - MIN_SCALE;
    float MARGIN_SCALE = 0.6f;
}