package me.thisnow.fragment;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import me.thisnow.R;
import me.thisnow.adapter.ObjectPresenter;
import me.thisnow.service.AppServerHandler;
import me.thisnow.service.util.Payload;
import me.thisnow.singleton.ObjectHolder;
import me.thisnow.singleton.Settings;
import me.thisnow.util.FusedLocation;
import me.thisnow.util.Message;
import me.thisnow.util.ViewHandler;
import me.thisnow.view.Gallery;

import static android.view.View.OnClickListener;
import static android.widget.AdapterView.OnItemClickListener;
import static me.thisnow.adapter.ObjectPresenter.RATIO_FACTOR;
import static me.thisnow.fragment.bundle.Parameter.MODE;
import static me.thisnow.service.constant.Action.GET_AROUND_ME_BUSINESS_OBJECTS;
import static me.thisnow.service.constant.Action.GET_AROUND_ME_EVENT_OBJECTS;
import static me.thisnow.service.constant.Action.GET_AROUND_ME_PERSON_OBJECTS;
import static me.thisnow.service.constant.Action.GET_TIMELINE_BUSINESS_OBJECTS;
import static me.thisnow.service.constant.Action.GET_TIMELINE_EVENT_OBJECTS;
import static me.thisnow.service.constant.Action.GET_TIMELINE_PERSON_OBJECTS;
import static me.thisnow.service.constant.Mode.AROUND_ME;
import static me.thisnow.service.constant.Mode.TIMELINE;
import static me.thisnow.service.constant.Path.OBJECTS_LIST;
import static me.thisnow.service.constant.Type.BUSINESS;
import static me.thisnow.service.constant.Type.EVENT;
import static me.thisnow.service.constant.Type.ABSENT;
import static me.thisnow.service.constant.Type.PERSON;
import static me.thisnow.service.util.ParametersBuilder.DismissValues.DISMISS_INT;
import static me.thisnow.service.util.ParametersBuilder.DismissValues.DISMISS_LONG;
import static me.thisnow.util.FusedLocation.UpdateListener;

public final class ObjectGrid extends Background implements OnItemClickListener, UpdateListener, OnClickListener {
    private final static String TAG = ObjectGrid.class.getSimpleName();

    private LinearLayout mObjectsGridHolder;
    private TextView mPersonObjectsTitle;
    private TextView mBusinessObjectsTitle;
    private TextView mEventObjectsTitle;
    private GridView mPersonObjectsGrid;
    private GridView mBusinessObjectsGrid;
    private GridView mEventObjectsGrid;

    private RelativeLayout mObjectsGalleryHolder;
    private Gallery mObjectsGallery;
    private ImageButton mVoteUp;
    private ImageButton mShare;
    private ImageButton mVoteDown;

    private GridViewEvent mPersonGridViewEvent;
    private GridViewEvent mBusinessGridViewEvent;
    private GridViewEvent mEventGridViewEvent;
    private int mode;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FusedLocation.getInstance().addListener(this);
        if (getArguments() != null && getArguments().containsKey(MODE)) {
            mode = getArguments().getInt(MODE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_object_grid, container, false);
        initViews(view);
        initListeners();
        initEvents(getActivity().getApplicationContext());
        initAdapters();
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        FusedLocation.getInstance().removeListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.gv_person_objects:
                setGalleryObjects(parent, view, position, PERSON);
                break;
            case R.id.gv_business_objects:
                setGalleryObjects(parent, view, position, BUSINESS);
                break;
            case R.id.gv_event_objects:
                setGalleryObjects(parent, view, position, EVENT);
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ib_object_vote_up:
                //TODO implement!
                Message.show(getActivity().getApplicationContext(), "Vote Up clicked!");
                break;
            case R.id.ib_object_share:
                //TODO implement!
                Message.show(getActivity().getApplicationContext(), "Share clicked!");
                break;
            case R.id.ib_object_vote_down:
                //TODO implement!
                Message.show(getActivity().getApplicationContext(), "Vote Down clicked!");
                break;
        }
    }

    @Override
    public void onSuccess(String action, String message) {

    }

    @Override
    public void onChange(Location location) {
        if (mode == TIMELINE) {
            requestObjects(mPersonObjectsGrid, mPersonGridViewEvent, location, GET_TIMELINE_PERSON_OBJECTS, DISMISS_INT);
            requestObjects(mBusinessObjectsGrid, mBusinessGridViewEvent, location, GET_TIMELINE_BUSINESS_OBJECTS, DISMISS_INT);
            requestObjects(mEventObjectsGrid, mEventGridViewEvent, location, GET_TIMELINE_EVENT_OBJECTS, DISMISS_INT);
        } else if (mode == AROUND_ME) {
            requestObjects(mPersonObjectsGrid, mPersonGridViewEvent, location, GET_AROUND_ME_PERSON_OBJECTS, Settings.getInstance().getRadius());
            requestObjects(mBusinessObjectsGrid, mBusinessGridViewEvent, location, GET_AROUND_ME_BUSINESS_OBJECTS, Settings.getInstance().getRadius());
            requestObjects(mEventObjectsGrid, mEventGridViewEvent, location, GET_AROUND_ME_EVENT_OBJECTS, Settings.getInstance().getRadius());
        }
    }

    @Override
    protected void initViews(View view) {
        mObjectsGridHolder = (LinearLayout) view.findViewById(R.id.objects_grid_holder);
        mPersonObjectsTitle = (TextView) view.findViewById(R.id.person_objects_title);
        mBusinessObjectsTitle = (TextView) view.findViewById(R.id.business_objects_title);
        mEventObjectsTitle = (TextView) view.findViewById(R.id.event_objects_title);
        mPersonObjectsGrid = (GridView) view.findViewById(R.id.gv_person_objects);
        mBusinessObjectsGrid = (GridView) view.findViewById(R.id.gv_business_objects);
        mEventObjectsGrid = (GridView) view.findViewById(R.id.gv_event_objects);

        mObjectsGalleryHolder = (RelativeLayout) view.findViewById(R.id.object_gallery_holder);
        mObjectsGallery = (Gallery) view.findViewById(R.id.objects_gallery);
        mVoteUp = (ImageButton) view.findViewById(R.id.ib_object_vote_up);
        mShare = (ImageButton) view.findViewById(R.id.ib_object_share);
        mVoteDown = (ImageButton) view.findViewById(R.id.ib_object_vote_down);
    }

    @Override
    protected void initListeners() {
        ViewHandler.setListListeners(this, mPersonObjectsGrid, mBusinessObjectsGrid, mEventObjectsGrid);
        ViewHandler.setClickListeners(this, mShare, mVoteUp, mVoteDown);
    }

    private void initEvents(Context context) {
        mPersonGridViewEvent = new GridViewEvent(context, mPersonObjectsGrid, mode, PERSON);
        mBusinessGridViewEvent = new GridViewEvent(context, mBusinessObjectsGrid, mode, BUSINESS);
        mEventGridViewEvent = new GridViewEvent(context, mEventObjectsGrid, mode, EVENT);
    }

    private void initAdapters() {
        postEvent(mPersonObjectsGrid, mPersonGridViewEvent);
        postEvent(mBusinessObjectsGrid, mBusinessGridViewEvent);
        postEvent(mEventObjectsGrid, mEventGridViewEvent);
    }

    private void postEvent(GridView gridView, Runnable runnable) {
        gridView.post(runnable);
    }

    private void requestObjects(GridView gridView, GridViewEvent event, Location location, String action, int radius) {
        if (gridView.getAdapter() == null) {
            gridView.setAdapter(new ObjectPresenter(event.context, event.mode, event.type, gridView.getColumnWidth()));
            AppServerHandler.post(getActivity().getApplicationContext(), action, OBJECTS_LIST,
                    Payload.getInstance().getForObjectsList(event.type, radius, DISMISS_LONG, DISMISS_LONG, location.getLatitude(), location.getLongitude()));
        }
    }

    private void setGalleryObjects(AdapterView parent, View view, int position, int type) {
        ViewHandler.hide(mPersonObjectsTitle, mBusinessObjectsTitle, mEventObjectsTitle);
        mObjectsGridHolder.getLayoutParams().height = getCollapsedGridHolderHeight((GridView) parent, view);
        mObjectsGallery.setObjects(mode, type);
        mObjectsGallery.setCurrentItem(position);
    }

    private int getCollapsedGridHolderHeight(GridView gridView, View view) {
        return view.getHeight() * 2 + gridView.getVerticalSpacing();
    }

    private static class GridViewEvent implements Runnable {
        private final Context context;
        private final GridView gridView;
        private final int mode;
        private final int type;

        public GridViewEvent(Context context, GridView gridView, int mode, int type) {
            this.context = context;
            this.gridView = gridView;
            this.mode = mode;
            this.type = type;
        }

        @Override
        public void run() {
            if (ObjectHolder.getInstance().getList(mode, type).size() != 0) {
                gridView.setAdapter(new ObjectPresenter(context, mode, type, gridView.getColumnWidth()));
            }
        }

    }

}