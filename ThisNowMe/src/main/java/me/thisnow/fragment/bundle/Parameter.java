package me.thisnow.fragment.bundle;

public interface Parameter {
    String BASE = Parameter.class.getCanonicalName();
    String MODE = BASE + ".Mode";
    String TYPE = BASE + ".Type";
    String POSITION = BASE + ".Position";
    String UPLOAD_TYPE = BASE + ".UploadType";
}