package me.thisnow.fragment.bundle;

import static me.thisnow.fragment.bundle.Parameter.MODE;

public final class ObjectGridArgs extends Arguments {

    public ObjectGridArgs(int mode) {
        data.putInt(MODE, mode);
    }

}