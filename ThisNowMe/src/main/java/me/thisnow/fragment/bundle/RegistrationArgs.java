package me.thisnow.fragment.bundle;

import static me.thisnow.fragment.bundle.Parameter.TYPE;

public final class RegistrationArgs extends Arguments {

    public RegistrationArgs(int type) {
        data.putInt(TYPE, type);
    }

}