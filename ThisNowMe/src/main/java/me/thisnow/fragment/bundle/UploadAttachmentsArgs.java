package me.thisnow.fragment.bundle;

import static me.thisnow.fragment.bundle.Parameter.UPLOAD_TYPE;

public final class UploadAttachmentsArgs extends Arguments {

    public UploadAttachmentsArgs(int uploadType) {
        data.putInt(UPLOAD_TYPE, uploadType);
    }

}