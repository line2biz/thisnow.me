package me.thisnow.fragment.bundle;

import static me.thisnow.fragment.bundle.Parameter.MODE;
import static me.thisnow.fragment.bundle.Parameter.POSITION;
import static me.thisnow.fragment.bundle.Parameter.TYPE;

public final class ObjectDetailPagerArgs extends Arguments {

    public ObjectDetailPagerArgs(int mode, int type, int position) {
        data.putInt(MODE, mode);
        data.putInt(TYPE, type);
        data.putInt(POSITION, position);
    }

}