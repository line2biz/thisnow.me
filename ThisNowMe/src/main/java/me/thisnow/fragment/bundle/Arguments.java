package me.thisnow.fragment.bundle;

import android.os.Bundle;

abstract class Arguments {
    protected Bundle data = new Bundle();

    public Bundle getData() {
        return data;
    }

}
