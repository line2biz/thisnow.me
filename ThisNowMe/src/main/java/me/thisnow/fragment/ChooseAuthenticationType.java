package me.thisnow.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import me.thisnow.R;
import me.thisnow.fragment.bundle.RegistrationArgs;
import me.thisnow.service.AppServerHandler;
import me.thisnow.service.util.Payload;
import me.thisnow.singleton.SubtypeHolder;
import me.thisnow.util.Network;

import static android.view.View.OnClickListener;
import static me.thisnow.constant.AppEvent.LOGIN_SUCCESS;
import static me.thisnow.constant.AppEvent.OPEN_LOGIN;
import static me.thisnow.constant.AppEvent.OPEN_REGISTRATION;
import static me.thisnow.service.constant.Action.GET_BUSINESS_SUBTYPES;
import static me.thisnow.service.constant.Action.GET_PERSON_SUBTYPES;
import static me.thisnow.service.constant.Action.LOGIN_NO_AUTH;
import static me.thisnow.service.constant.Path.NO_AUTH;
import static me.thisnow.service.constant.Path.TYPE_BASE;
import static me.thisnow.service.constant.Type.BUSINESS;
import static me.thisnow.service.constant.Type.PERSON;

public final class ChooseAuthenticationType extends Background implements OnClickListener {
    private Button mLogin;
    private Button mAnonymous;
    private Button mPersonSignUp;
    private Button mBusinessSignUp;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_choose_authentication_type, container, false);
        initViews(view);
        initListeners();
        return view;
    }

    @Override
    protected void initViews(View view) {
        mLogin = (Button) view.findViewById(R.id.btn_login);
        mAnonymous = (Button) view.findViewById(R.id.btn_anonymous);
        mPersonSignUp = (Button) view.findViewById(R.id.btn_sign_up_person);
        mBusinessSignUp = (Button) view.findViewById(R.id.btn_sign_up_business);
    }

    @Override
    protected void initListeners() {
        mLogin.setOnClickListener(this);
        mAnonymous.setOnClickListener(this);
        mPersonSignUp.setOnClickListener(this);
        mBusinessSignUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                sendEvent(OPEN_LOGIN, null);
                break;
            case R.id.btn_anonymous:
                if (Network.isConnected(getActivity().getApplicationContext())) {
                    AppServerHandler.post(getActivity().getApplicationContext(), LOGIN_NO_AUTH, NO_AUTH,
                            Payload.getInstance().getForNoAuth(getActivity().getApplicationContext()));
                } else {
                    Network.checkConnectionMessage(getActivity().getApplicationContext());
                }
                break;
            case R.id.btn_sign_up_person:
                handleClick(OPEN_REGISTRATION, PERSON, GET_PERSON_SUBTYPES);
                break;
            case R.id.btn_sign_up_business:
                handleClick(OPEN_REGISTRATION, BUSINESS, GET_BUSINESS_SUBTYPES);
                break;
        }
    }

    @Override
    public void onSuccess(String action, String message) {
        if (LOGIN_NO_AUTH.equals(action)) {
            sendEvent(LOGIN_SUCCESS, null);
        } else if (GET_PERSON_SUBTYPES.equals(action)) {
            sendEvent(OPEN_REGISTRATION, new RegistrationArgs(PERSON).getData());
        } else if (GET_BUSINESS_SUBTYPES.equals(action)) {
            sendEvent(OPEN_REGISTRATION, new RegistrationArgs(BUSINESS).getData());
        }
    }

    private void handleClick(int event, int type, String action) {
        if (SubtypeHolder.getInstance().getSubtypeCount(type) > 0) {
            sendEvent(event, new RegistrationArgs(type).getData());
        } else {
            if (Network.isConnected(getActivity().getApplicationContext())) {
                AppServerHandler.get(getActivity().getApplicationContext(), action, TYPE_BASE + type);
            } else {
                Network.checkConnectionMessage(getActivity().getApplicationContext());
            }
        }
    }

}