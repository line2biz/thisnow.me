package me.thisnow.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.View;

public abstract class Basic extends Fragment {
    private OnInteractionListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement " + OnInteractionListener.class.getSimpleName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    protected abstract void initViews(View view);

    protected abstract void initListeners();

    protected void sendEvent(int event, Bundle args) {
        if (mListener != null) {
            mListener.onEvent(event, args);
        }
    }

}