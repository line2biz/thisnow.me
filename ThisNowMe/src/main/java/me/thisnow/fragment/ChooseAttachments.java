package me.thisnow.fragment;

import android.annotation.TargetApi;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.makeramen.RoundedImageView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import me.thisnow.R;
import me.thisnow.constant.Char;
import me.thisnow.database.model.Attachment;
import me.thisnow.fragment.bundle.Parameter;
import me.thisnow.fragment.bundle.UploadAttachmentsArgs;
import me.thisnow.singleton.AccountInfoHolder;
import me.thisnow.singleton.ObjectHolder;
import me.thisnow.singleton.UploadAttachmentsHolder;
import me.thisnow.util.Message;
import me.thisnow.util.Validator;

import static android.app.Activity.RESULT_OK;
import static android.content.Intent.ACTION_GET_CONTENT;
import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.KITKAT;
import static android.os.Environment.DIRECTORY_PICTURES;
import static android.provider.BaseColumns._ID;
import static android.provider.MediaStore.ACTION_IMAGE_CAPTURE;
import static android.provider.MediaStore.ACTION_VIDEO_CAPTURE;
import static android.provider.MediaStore.EXTRA_OUTPUT;
import static android.provider.MediaStore.MediaColumns.DATA;
import static android.provider.MediaStore.Video.Thumbnails.MINI_KIND;
import static android.view.View.LAYER_TYPE_SOFTWARE;
import static me.thisnow.constant.AppEvent.OPEN_UPLOAD_ATTACHMENTS;
import static me.thisnow.constant.Char.COLON;
import static me.thisnow.constant.Char.UNDERLINE;
import static me.thisnow.database.model.Attachment.CONTENT_URI;
import static me.thisnow.database.model.Attachment.OBJECT_ID;
import static me.thisnow.database.model.Attachment.TOKEN;
import static me.thisnow.fragment.ChooseAttachments.MediaType.IMAGE;
import static me.thisnow.fragment.ChooseAttachments.MediaType.VIDEO;
import static me.thisnow.fragment.UploadAttachments.REGISTRATION;

public final class ChooseAttachments extends Basic implements View.OnClickListener {
    private static final String TAG = ChooseAttachments.class.getSimpleName();
    private static final String DATE_FORMAT = "yyyyMMdd_HHmmss";
    private static final String IMG_PREFIX = "IMG" + UNDERLINE;
    private static final String EXTENSION_JPG = Char.DOT + "jpg";
    private static final String CHOOSER_TITLE = "Choose application:";
    private static final String CONTENT = "content";
    private static final String IMAGE_TYPE = "image/*";
    private static final String VIDEO_TYPE = "video/*";
    private static final int PICK_IMAGE = 0x0101;
    private static final int PICK_VIDEO = 0x0111;
    private static final int CAPTURE_PHOTO = 0x0121;
    private static final int CAPTURE_VIDEO = 0x0131;
    private static final int MAX_PHOTO_COUNT = 5;
    private static final int MAX_VIDEO_COUNT = 1;
    private final Validator validator;
    private File imageFile;
    private LinearLayout mPhotoHolder;
    private LinearLayout mVideoHolder;
    private Button mAddPhoto;
    private Button mCapturePhoto;
    private Button mAddVideo;
    private Button mCaptureVideo;
    private ImageButton mSubmit;
    private int mColor;

    public ChooseAttachments() {
        validator = new Validator();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(Parameter.TYPE)) {
            mColor = ObjectHolder.getInstance().getBorderColor(getActivity().getApplicationContext(), getArguments().getInt(Parameter.TYPE));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_choose_attachments, container, false);
        initViews(view);
        initListeners();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        removePreviewsFromHolder(mPhotoHolder);
        removePreviewsFromHolder(mVideoHolder);
    }

    @Override
    protected void initViews(View view) {
        mPhotoHolder = (LinearLayout) view.findViewById(R.id.ll_photo_holder);
        mVideoHolder = (LinearLayout) view.findViewById(R.id.ll_video_holder);
        mAddPhoto = (Button) view.findViewById(R.id.btn_pick_photo);
        mAddVideo = (Button) view.findViewById(R.id.btn_pick_video);
        mCapturePhoto = (Button) view.findViewById(R.id.btn_capture_photo);
        mCaptureVideo = (Button) view.findViewById(R.id.btn_capture_video);
        mSubmit = (ImageButton) view.findViewById(R.id.ib_attachments_submit);
    }

    @Override
    protected void initListeners() {
        mAddPhoto.setOnClickListener(this);
        mAddVideo.setOnClickListener(this);
        mCapturePhoto.setOnClickListener(this);
        mCaptureVideo.setOnClickListener(this);
        mSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_pick_photo:
            case R.id.btn_capture_photo:
                checkThenGetFromDevice(mPhotoHolder, MAX_PHOTO_COUNT, view.getId(), getString(R.string.error_photo_attachments_exceed_limit));
                break;
            case R.id.btn_pick_video:
            case R.id.btn_capture_video:
                checkThenGetFromDevice(mVideoHolder, MAX_VIDEO_COUNT, view.getId(), getString(R.string.error_video_attachments_exceed_limit));
                break;
            case R.id.iv_attachment:
                removePreview((ImageView) view);
                break;
            case R.id.ib_attachments_submit:
                checkThenSubmit();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            String path = getPath(data, requestCode);
            if (validator.notNull(path)) {
                addAttachment(path, requestCode);
            } else {
                Message.show(getActivity().getApplicationContext(), R.string.error_retrieve_path);
            }
            Log.d(TAG, "path to file: " + path);
        }
    }

    private void checkThenSubmit() {
        if (validator.childCountNotEqual(mPhotoHolder, 0)) {
            if (!UploadAttachmentsHolder.getInstance().getHandlerStopState()) {
                submitAll();
            } else {
                Message.show(getActivity().getApplicationContext(), R.string.error_wait_upload_stop);
            }
        } else {
            Message.show(getActivity().getApplicationContext(), R.string.error_no_photo_attachments);
        }
    }

    private void submitAll() {
        getActivity().getContentResolver().delete(CONTENT_URI, TOKEN + "=?) AND (" + OBJECT_ID + "=?",
                new String[]{AccountInfoHolder.getInstance().getToken(), AccountInfoHolder.getInstance().getObjectId()});
        submitAttachmentsFromHolder(mPhotoHolder, IMAGE);
        submitAttachmentsFromHolder(mVideoHolder, VIDEO);
        sendEvent(OPEN_UPLOAD_ATTACHMENTS, new UploadAttachmentsArgs(REGISTRATION).getData());
    }

    private void submitAttachmentsFromHolder(ViewGroup holder, String type) {
        String token = AccountInfoHolder.getInstance().getToken();
        String objectId = AccountInfoHolder.getInstance().getObjectId();
        for (int i = 0; i < holder.getChildCount(); i++) {
            Attachment attachment = new Attachment();
            attachment.setToken(token);
            attachment.setObjectId(objectId);
            attachment.setPath((String) holder.getChildAt(i).getTag());
            attachment.setMediaType(type);
            UploadAttachmentsHolder.getInstance().insert(getActivity().getApplicationContext(), attachment.getContentValues());
        }
    }

    private void removePreviewsFromHolder(ViewGroup holder) {
        for (int i = 0; i < holder.getChildCount(); i++) {
            removePreview((ImageView) holder.getChildAt(i));
        }
    }

    private void removePreview(ImageView preview) {
        preview.setImageDrawable(null);
        ((ViewGroup) preview.getParent()).removeView(preview);
    }

    private void checkThenGetFromDevice(ViewGroup holder, int count, int id, String error) {
        if (validator.childCountNotEqual(holder, count)) {
            getFromDevice(holder.getId(), id);
        } else {
            Message.show(getActivity().getApplicationContext(), error);
        }
    }

    private void getFromDevice(int holderId, int id) {
        if (holderId == R.id.ll_photo_holder && id == R.id.btn_pick_photo) {
            pick(IMAGE_TYPE, PICK_IMAGE);
        } else if (holderId == R.id.ll_photo_holder && id == R.id.btn_capture_photo) {
            capture(ACTION_IMAGE_CAPTURE, CAPTURE_PHOTO);
        } else if (holderId == R.id.ll_video_holder && id == R.id.btn_pick_video) {
            pick(VIDEO_TYPE, PICK_VIDEO);
        } else if (holderId == R.id.ll_video_holder && id == R.id.btn_capture_video) {
            capture(ACTION_VIDEO_CAPTURE, CAPTURE_VIDEO);
        }
    }

    private void pick(String type, int requestCode) {
        Intent intent = new Intent(ACTION_GET_CONTENT);
        intent.setType(type);
        checkThenSendIntent(intent, requestCode);
    }

    private void capture(String action, int requestCode) {
        Intent intent = new Intent(action);
        if (requestCode == CAPTURE_PHOTO) {
            initImageFile(IMG_PREFIX + getCurrentDate() + EXTENSION_JPG);
            intent.putExtra(EXTRA_OUTPUT, Uri.fromFile(imageFile));
        }
        checkThenSendIntent(intent, requestCode);
    }

    private void initImageFile(String filename) {
        imageFile = new File(initExternalDir(DIRECTORY_PICTURES), filename);
    }

    private String getCurrentDate() {
        return new SimpleDateFormat(DATE_FORMAT).format(new Date());
    }

    private File initExternalDir(String type) {
        File imageDir = Environment.getExternalStoragePublicDirectory(type);
        if (!imageDir.exists()) {
            imageDir.mkdirs();
        }
        return imageDir;
    }

    private void checkThenSendIntent(Intent intent, int requestCode) {
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(Intent.createChooser(intent, CHOOSER_TITLE), requestCode);
        }
    }

    private String getPath(Intent intent, int requestCode) {
        String path = null;
        if (requestCode == PICK_IMAGE) {
            path = getPathFromUri(intent.getData(), MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        } else if (requestCode == CAPTURE_PHOTO) {
            path = imageFile.getAbsolutePath();
        } else if (requestCode == PICK_VIDEO || requestCode == CAPTURE_VIDEO) {
            path = getPathFromUri(intent.getData(), MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        }
        return path;
    }

    public String getPathFromUri(Uri fileUri, Uri contentUri) {
        Log.d(TAG, "file uri: " + fileUri);
        if (CONTENT.equals(fileUri.getScheme())) {
            if (SDK_INT >= KITKAT) {
                return getPathFromDocument(fileUri, contentUri);
            } else {
                return getPathFromProviderByUri(fileUri);
            }
        } else {
            return fileUri.getPath();
        }
    }

    @TargetApi(KITKAT)
    private String getPathFromDocument(Uri uri, Uri contentUri) {
        return (DocumentsContract.isDocumentUri(getActivity().getApplicationContext(), uri)) ?
                getPathFromProviderById(uri, contentUri) : getPathFromProviderByUri(uri);
    }

    @TargetApi(KITKAT)
    private String getPathFromProviderById(Uri uri, Uri contentUri) {
        String path = null;
        String documentId = DocumentsContract.getDocumentId(uri);
        Cursor cursor = getActivity().getContentResolver().query(contentUri, new String[]{DATA},
                _ID + "=?", new String[]{documentId.substring(documentId.lastIndexOf(COLON) + 1)}, null);
        if (cursor != null && cursor.moveToFirst()) {
            path = cursor.getString(cursor.getColumnIndex(DATA));
            cursor.close();
        }
        return path;
    }

    private String getPathFromProviderByUri(Uri uri) {
        String path = null;
        Cursor cursor = getActivity().getContentResolver().query(uri, new String[]{DATA}, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            path = cursor.getString(cursor.getColumnIndex(DATA));
            cursor.close();
        }
        return path;
    }

    private void addAttachment(String path, int requestCode) {
        Bitmap thumbnail = getThumbnail(path, requestCode);
        ViewGroup holder = getHolder(requestCode);
        if (validator.notNull(thumbnail) & validator.notNull(holder)) {
            if (!fileExistInHolder(holder, path)) {
                addThumbnailToHolder(holder, thumbnail, path);
            } else {
                Message.show(getActivity().getApplicationContext(), R.string.error_file_exist);
            }
        } else {
            Message.show(getActivity().getApplicationContext(), R.string.error_fail_retrieve_thumbnail);
        }
    }

    private Bitmap getThumbnail(String path, int requestCode) {
        if (requestCode == PICK_IMAGE || requestCode == CAPTURE_PHOTO) {
            return getImageBitmap(path);
        } else if (requestCode == PICK_VIDEO || requestCode == CAPTURE_VIDEO) {
            return ThumbnailUtils.createVideoThumbnail(path, MINI_KIND);
        }
        return null;
    }

    private Bitmap getImageBitmap(String path) {
        int requestSize = getResources().getDimensionPixelSize(R.dimen.attachment_size);
        return decodeFile(path, requestSize, requestSize);
    }

    public Bitmap decodeFile(String path, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        options.inSampleSize = getInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }

    public int getInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    private ViewGroup getHolder(int requestCode) {
        if (requestCode == PICK_IMAGE || requestCode == CAPTURE_PHOTO) {
            return mPhotoHolder;
        } else if (requestCode == PICK_VIDEO || requestCode == CAPTURE_VIDEO) {
            return mVideoHolder;
        }
        return null;
    }

    private boolean fileExistInHolder(ViewGroup holder, String path) {
        for (int i = 0; i < holder.getChildCount(); i++) {
            if (path.equals(holder.getChildAt(i).getTag())) {
                return true;
            }
        }
        return false;
    }

    private void addThumbnailToHolder(ViewGroup holder, Bitmap thumbnail, String path) {
        RoundedImageView preview = (RoundedImageView) LayoutInflater.from(getActivity().getApplicationContext()).inflate(R.layout.attachment_item, null);
        preview.setLayerType(LAYER_TYPE_SOFTWARE, null);
        preview.setImageBitmap(thumbnail);
        preview.setOnClickListener(this);
        preview.setBorderColor(mColor);
        preview.setTag(path);
        holder.addView(preview);
    }

    static interface MediaType {
        String IMAGE = "image";
        String VIDEO = "video";
    }
}