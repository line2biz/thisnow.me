package me.thisnow.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import me.thisnow.R;
import me.thisnow.util.Preferences;

import static me.thisnow.constant.AppEvent.OPEN_AUTHENTICATION;

public final class AppInfo extends Basic implements View.OnClickListener {
    private ImageButton mGo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_app_info, container, false);
        initViews(view);
        initListeners();
        return view;
    }

    @Override
    protected void initViews(View view) {
        mGo = (ImageButton) view.findViewById(R.id.ib_go);
    }

    @Override
    protected void initListeners() {
        mGo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ib_go:
                Preferences.setAppInfoShown(getActivity().getApplicationContext(), true);
                sendEvent(OPEN_AUTHENTICATION, null);
                break;
        }
    }

}