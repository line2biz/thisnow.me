package me.thisnow.fragment;

import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import me.thisnow.service.ServerCallback;
import me.thisnow.util.Message;

public abstract class Background extends Basic implements ServerCallback.EventListener {
    private static final String TAG = Background.class.getSimpleName();
    private ServerCallback serverCallback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        serverCallback = new ServerCallback(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(serverCallback, serverCallback.getFilter());
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(serverCallback);
    }

    @Override
    public void onSuccess(String action, String message) {
        Message.show(getActivity().getApplicationContext(), message);
    }

    @Override
    public void onError(String action, String message) {
        Message.show(getActivity().getApplicationContext(), message);
    }

}