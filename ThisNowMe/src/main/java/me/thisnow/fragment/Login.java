package me.thisnow.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import me.thisnow.R;
import me.thisnow.fragment.bundle.UploadAttachmentsArgs;
import me.thisnow.service.AppServerHandler;
import me.thisnow.service.util.Payload;
import me.thisnow.singleton.AccountInfoHolder;
import me.thisnow.singleton.UploadAttachmentsHolder;
import me.thisnow.util.Encrypt;
import me.thisnow.util.Message;
import me.thisnow.util.Network;
import me.thisnow.util.Validator;

import static me.thisnow.constant.AppEvent.LOGIN_SUCCESS;
import static me.thisnow.constant.AppEvent.OPEN_UPLOAD_ATTACHMENTS;
import static me.thisnow.fragment.UploadAttachments.LOGIN;
import static me.thisnow.service.constant.Action.LOGIN_AUTH;
import static me.thisnow.service.constant.Path.AUTH;

public final class Login extends Background implements View.OnClickListener {
    private static final String TAG = Login.class.getSimpleName();
    private final Validator validator;
    private EditText mLogin;
    private EditText mPassword;
    private ImageButton mGo;

    public Login() {
        validator = new Validator();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        initViews(view);
        initListeners();
        return view;
    }

    @Override
    protected void initViews(View view) {
        mLogin = (EditText) view.findViewById(R.id.et_username);
        mPassword = (EditText) view.findViewById(R.id.et_password);
        mGo = (ImageButton) view.findViewById(R.id.ib_go);
    }

    @Override
    protected void initListeners() {
        mGo.setOnClickListener(this);
    }

    @Override
    public void onSuccess(String action, String message) {
        super.onSuccess(action, message);
        if (LOGIN_AUTH.equals(action)) {
            checkDatabaseThenSendEvent();
        }
        Log.d(TAG, "token: " + AccountInfoHolder.getInstance().getToken());
        Log.d(TAG, "object ID: " + AccountInfoHolder.getInstance().getObjectId());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ib_go:
                String error = getString(R.string.error_empty_field);
                if (validator.notEmpty(mLogin, error) & validator.notEmpty(mPassword, error)) {
                    final String login = mLogin.getText().toString();
                    final String passwordMD5 = Encrypt.md5(mPassword.getText().toString());
                    if (validator.notNull(mPassword, passwordMD5, getString(R.string.error_change_field))) {
                        if (Network.isConnected(getActivity().getApplicationContext())) {
                            AppServerHandler.post(getActivity().getApplicationContext(), LOGIN_AUTH, AUTH,
                                    Payload.getInstance().getForAuth(getActivity().getApplicationContext(), login, passwordMD5));
                        } else {
                            Network.checkConnectionMessage(getActivity().getApplicationContext());
                        }
                    }
                }
                break;
        }
    }

    private void checkDatabaseThenSendEvent() {
        Cursor cursor = UploadAttachmentsHolder.getInstance().getAll(getActivity().getApplicationContext());
        if (cursor != null && cursor.moveToFirst()) {
            if (!UploadAttachmentsHolder.getInstance().getHandlerStopState()) {
                sendEvent(OPEN_UPLOAD_ATTACHMENTS, new UploadAttachmentsArgs(LOGIN).getData());
            } else {
                Message.show(getActivity().getApplicationContext(), R.string.error_wait_upload_stop);
            }
            cursor.close();
        } else {
            sendEvent(LOGIN_SUCCESS, null);
        }
    }

}