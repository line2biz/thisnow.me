package me.thisnow.fragment;

import android.os.Bundle;

public interface OnInteractionListener {
    public void onEvent(int event, Bundle args);
}