package me.thisnow.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;

import me.thisnow.R;
import me.thisnow.adapter.SubtypePresenter;
import me.thisnow.fragment.bundle.Parameter;
import me.thisnow.service.AppServerHandler;
import me.thisnow.service.model.geocoding.Result;
import me.thisnow.service.GeocoderHandler;
import me.thisnow.singleton.AccountInfoHolder;
import me.thisnow.singleton.GeocoderHolder;
import me.thisnow.singleton.SubtypeHolder;
import me.thisnow.util.Encrypt;
import me.thisnow.util.Network;
import me.thisnow.service.util.Payload;
import me.thisnow.util.Validator;

import static me.thisnow.constant.AppEvent.REGISTRATION_SUCCESS;
import static me.thisnow.service.constant.Action.REGISTER_PERSON;
import static me.thisnow.service.constant.Action.REGISTER_BUSINESS;
import static me.thisnow.service.constant.Action.REGISTER_EVENT;
import static me.thisnow.service.constant.Action.GET_ADDRESS_LOCATION;
import static me.thisnow.service.constant.Type.PERSON;
import static me.thisnow.service.constant.Type.BUSINESS;
import static me.thisnow.service.constant.Type.EVENT;
import static me.thisnow.service.constant.Path.REGISTER;

public final class Registration extends Background implements View.OnClickListener {
    private static final String TAG = Registration.class.getSimpleName();
    private final Validator validator;
    private LinearLayout mLastNameHolder;
    private EditText mName;
    private EditText mLastName;
    private EditText mAddress;
    private EditText mLogin;
    private EditText mPassword;
    private SubtypePresenter mSubtypePresenter;
    private Spinner mSubtypes;
    private ImageButton mGo;
    private Bundle data;
    private int type;

    public Registration() {
        validator = new Validator();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(Parameter.TYPE)) {
            type = getArguments().getInt(Parameter.TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration, container, false);
        initViews(view);
        initListeners();
        initAdapters();
        initVisibility();
        return view;
    }

    @Override
    protected void initViews(View view) {
        mLastNameHolder = (LinearLayout) view.findViewById(R.id.holder_person_last_name);
        mName = (EditText) view.findViewById(R.id.et_person_name);
        mLastName = (EditText) view.findViewById(R.id.et_person_last_name);
        mAddress = (EditText) view.findViewById(R.id.et_person_address);
        mLogin = (EditText) view.findViewById(R.id.et_person_login);
        mPassword = (EditText) view.findViewById(R.id.et_person_password);
        mSubtypes = (Spinner) view.findViewById(R.id.sp_person_subtypes);
        mGo = (ImageButton) view.findViewById(R.id.ib_go);
    }

    @Override
    protected void initListeners() {
        mGo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ib_go:
                final String error = getString(R.string.error_empty_field);
                if (validator.notEmpty(mLogin, error) & validator.notEmpty(mPassword, error) & validator.notEmpty(mName, error) & validator.notEmpty(mAddress, error)) {
                    final String passwordMD5 = Encrypt.md5(mPassword.getText().toString());
                    if (validator.notNull(mPassword, passwordMD5, getString(R.string.error_change_field))) {
                        if (Network.isConnected(getActivity().getApplicationContext())) {

                            //FIXME remove fake values and get from custom view
                            String[] keywords = new String[] {"restaurant", "luxury place"};

                            data = Payload.getInstance().getForRegistration(getActivity().getApplicationContext(),
                                    getText(mLogin), passwordMD5, getText(mName), getText(mLastName), getText(mAddress),
                                    type, mSubtypePresenter.getItem(mSubtypes.getSelectedItemPosition()).getCode(), keywords);
                            GeocoderHandler.getLocationFromAddress(getActivity().getApplicationContext(), mAddress.getText().toString());
                        } else {
                            Network.checkConnectionMessage(getActivity().getApplicationContext());
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onSuccess(String action, String message) {
        super.onSuccess(action, message);
        if (REGISTER_PERSON.equals(action) || REGISTER_BUSINESS.equals(action) || REGISTER_EVENT.equals(action)) {
            sendEvent(REGISTRATION_SUCCESS, getArguments());
            Log.d(TAG, "token: " + AccountInfoHolder.getInstance().getToken());
            Log.d(TAG, "object ID: " + AccountInfoHolder.getInstance().getObjectId());
        } else if (GET_ADDRESS_LOCATION.equals(action)) {
            Result result = GeocoderHolder.getInstance().getResults().get(0);
            Payload.getInstance().putLocation(data, result.getGeometry().getLocation().getLatitude(), result.getGeometry().getLocation().getLongitude());
            AppServerHandler.post(getActivity().getApplicationContext(), getAction(type), REGISTER, data);
        }
    }

    private void initAdapters() {
        mSubtypePresenter = new SubtypePresenter(getActivity(), R.id.tv_subtype_name, SubtypeHolder.getInstance().getSubtypes(type));
        mSubtypes.setAdapter(mSubtypePresenter);
    }

    private void initVisibility() {
        if (type == BUSINESS || type == EVENT){
            mLastNameHolder.setVisibility(View.GONE);
        }
    }

    private String getAction(int type) {
        switch (type) {
            case PERSON:
                return REGISTER_PERSON;
            case BUSINESS:
                return REGISTER_BUSINESS;
            case EVENT:
                return REGISTER_EVENT;
        }
        return null;
    }

    private String getText(EditText field) {
        return field.getText().toString();
    }

}