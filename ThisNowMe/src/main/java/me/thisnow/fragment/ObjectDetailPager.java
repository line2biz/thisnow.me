package me.thisnow.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import me.thisnow.R;
import me.thisnow.view.Gallery;

import static me.thisnow.fragment.bundle.Parameter.MODE;
import static me.thisnow.fragment.bundle.Parameter.POSITION;
import static me.thisnow.fragment.bundle.Parameter.TYPE;

public final class ObjectDetailPager extends Basic {
    private static final String TAG = ObjectDetailPager.class.getSimpleName();
    private Gallery mObjectGallery;
    private int mode;
    private int type;
    private int position;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null
                && getArguments().containsKey(MODE)
                && getArguments().containsKey(TYPE)
                && getArguments().containsKey(POSITION)) {
            mode = getArguments().getInt(MODE);
            type = getArguments().getInt(TYPE);
            position = getArguments().getInt(POSITION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_object_detail_pager, container, false);
        initViews(view);
        initListeners();
        return view;
    }

    @Override
    protected void initViews(View view) {
        mObjectGallery = (Gallery) view.findViewById(R.id.object_detail_gallery);
        mObjectGallery.setObjects(mode, type);
        mObjectGallery.setCurrentItem(position);
    }

    @Override
    protected void initListeners() {

    }

}