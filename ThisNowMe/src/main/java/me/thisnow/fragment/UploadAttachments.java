package me.thisnow.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import me.thisnow.R;
import me.thisnow.service.UploadAttachmentHandler;
import me.thisnow.singleton.UploadAttachmentsHolder;
import me.thisnow.util.Message;

import static android.view.View.GONE;
import static android.view.View.OnClickListener;
import static android.view.View.VISIBLE;
import static me.thisnow.constant.AppEvent.ATTACHMENT_UPLOAD__SUCCESS;
import static me.thisnow.fragment.bundle.Parameter.UPLOAD_TYPE;
import static me.thisnow.service.constant.Action.AMAZON_UPLOAD;
import static me.thisnow.singleton.UploadAttachmentsHolder.Observer;
import static me.thisnow.singleton.UploadAttachmentsHolder.Key.CURRENT;
import static me.thisnow.singleton.UploadAttachmentsHolder.Key.MAX;

public final class UploadAttachments extends Background implements OnClickListener, Observer {
    private static final String TAG = UploadAttachments.class.getSimpleName();
    public static final int REGISTRATION = 0x2201;
    public static final int LOGIN = 0x2201;
    private ProgressBar mUploadProgress;
    private View mRestartUpload;
    private int uploadType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(UPLOAD_TYPE)) {
            uploadType = getArguments().getInt(UPLOAD_TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_upload_attachments, container, false);
        initViews(view);
        initListeners();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            initUpload();
        } else {
            mUploadProgress.setMax(UploadAttachmentsHolder.getInstance().getMax());
            mUploadProgress.setProgress(UploadAttachmentsHolder.getInstance().getCurrent());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        UploadAttachmentsHolder.getInstance().registerObserver(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        UploadAttachmentsHolder.getInstance().unregisterObserver(this);
    }

    @Override
    protected void initViews(View view) {
        mUploadProgress = (ProgressBar) view.findViewById(R.id.pb_upload);
        mRestartUpload = view.findViewById(R.id.btn_restart_upload);
    }

    @Override
    protected void initListeners() {
        mRestartUpload.setOnClickListener(this);
    }

    @Override
    public void onSuccess(String action, String message) {
        if (AMAZON_UPLOAD.equals(action)) {
            sendEvent(ATTACHMENT_UPLOAD__SUCCESS, null);
        }
    }

    @Override
    public void onError(String action, String message) {
        showUploadRestartButton(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_restart_upload:
                if (!UploadAttachmentsHolder.getInstance().getHandlerStopState()) {
                    showUploadRestartButton(false);
                    initUpload();
                } else {
                    Message.show(getActivity().getApplicationContext(), R.string.error_wait_upload_stop);
                }
                break;
        }
    }

    @Override
    public void onUpdate(int key, int value) {
        switch (key) {
            case MAX:
                Log.d(TAG, "upload: max " + value);
                mUploadProgress.setMax(value);
                break;
            case CURRENT:
                Log.d(TAG, "upload: current " + value);
                mUploadProgress.setProgress(value);
                break;
        }
    }

    private void initUpload() {
        Log.d(TAG, "upload: init!");
        UploadAttachmentHandler.upload(getActivity().getApplicationContext());
    }

    private void showUploadRestartButton(boolean state) {
        mUploadProgress.setVisibility(state ? GONE : VISIBLE);
        mRestartUpload.setVisibility(state ? VISIBLE : GONE);
    }

}