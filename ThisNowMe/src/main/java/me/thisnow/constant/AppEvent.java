package me.thisnow.constant;

public interface AppEvent {
    int OPEN_AUTHENTICATION = 0x0011;
    int OPEN_LOGIN = 0x0021;
    int OPEN_REGISTRATION = 0x0041;
    int OPEN_UPLOAD_ATTACHMENTS = 0x0051;
    int OPEN_BLOCK_DETAIL_PAGER = 0x0071;
    int LOGIN_SUCCESS = 0x0101;
    int REGISTRATION_SUCCESS = 0x0201;
    int ATTACHMENT_UPLOAD__SUCCESS = 0x0301;
}