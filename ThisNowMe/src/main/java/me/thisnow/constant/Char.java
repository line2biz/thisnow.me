package me.thisnow.constant;

public interface Char {
    String EQUAL = "=";
    String AND = "&";
    String COLON = ":";
    String UNDERLINE = "_";
    String DOT = ".";
}