package me.thisnow.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import me.thisnow.R;
import me.thisnow.service.model.Subtype;

public final class SubtypePresenter extends ArrayAdapter<Subtype> {
    private final LayoutInflater mInflater;
    private final ArrayList<Subtype> mItems;

    public SubtypePresenter(Context context, int textViewResourceId, ArrayList<Subtype> items) {
        super(context, textViewResourceId, items);
        mInflater = LayoutInflater.from(context);
        mItems = items;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        return getCustomView(pos, convertView, parent);
    }

    private View getCustomView(int position, View convertView, ViewGroup parent) {
        SubtypeItemHolder holder;
        if(convertView==null){
            holder = new SubtypeItemHolder();
            convertView = mInflater.inflate(R.layout.subtype_list_item, parent, false);
            holder.subTypeName = (TextView) convertView.findViewById(R.id.tv_subtype_name);
            convertView.setTag(holder);
        } else {
            holder = (SubtypeItemHolder) convertView.getTag();
        }
        holder.subTypeName.setText(mItems.get(position).getName());
        return convertView;
    }

    private static class SubtypeItemHolder {
        TextView subTypeName;
    }

}