package me.thisnow.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.makeramen.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.imageaware.ImageViewAware;

import java.util.ArrayList;

import me.thisnow.R;
import me.thisnow.singleton.ObjectHolder;
import me.thisnow.singleton.model.ObjectItem;
import me.thisnow.util.Graphics;

import static me.thisnow.singleton.ObjectHolder.Observer;

public final class ObjectPresenter extends BaseAdapter implements ImageLoadingListener, Observer {
    public static final float RATIO_FACTOR = 0.8f;
    private static final String TAG = ObjectPresenter.class.getSimpleName();
    private final LayoutInflater mInflater;
    private final UpdateObjectItems mUpdateObjectItems;
    private final Handler mHandler;
    private final int mWidth;
    private final int mHeight;
    private final int mColor;
    private ArrayList<ObjectItem> mObjectItems;

    public ObjectPresenter(Context context, int mode, int type, int width) {
        mObjectItems = ObjectHolder.getInstance().getList(mode, type);
        mInflater = LayoutInflater.from(context);
        mUpdateObjectItems = new UpdateObjectItems(this, mode, type);
        mHandler = new Handler();
        mWidth = width;
        mHeight = (int) (width * RATIO_FACTOR);
        mColor = ObjectHolder.getInstance().getBorderColor(context, type);
        ObjectHolder.getInstance().registerObserver(this);
    }

    @Override
    public int getCount() {
        return mObjectItems.size();
    }

    @Override
    public ObjectItem getItem(int position) {
        return mObjectItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mObjectItems.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PresenterItemHolder holder;
        if (convertView == null) {
            holder = new PresenterItemHolder();
            convertView = mInflater.inflate(R.layout.object_grid_item, parent, false);
            holder.image = (RoundedImageView) convertView.findViewById(R.id.riv_grid_item);
            holder.image.setBorderColor(mColor);
            holder.width = mWidth;
            holder.height = mHeight;
            convertView.setTag(holder);
        } else {
            holder = (PresenterItemHolder) convertView.getTag();
        }
        ImageLoader.getInstance().displayImage(getItem(position).getImage(), new ImageViewAware(holder.image, false), this);
        return convertView;
    }

    @Override
    public void onLoadingStarted(String imageUri, View view) {

    }

    @Override
    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
        showBitmap((PresenterItemHolder) view.getTag(), null);
    }

    @Override
    public void onLoadingCancelled(String imageUri, View view) {
        showBitmap((PresenterItemHolder) view.getTag(), null);
    }

    @Override
    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
        PresenterItemHolder holder = (PresenterItemHolder) view.getTag();
        holder.image.setImageBitmap(Graphics.getInstance().getBitmapScaledToWidth(loadedImage, holder.width, holder.height));
    }

    @Override
    public void onUpdate(int key, int value) {
        if (key == mUpdateObjectItems.getMode()) {
            if (value == mUpdateObjectItems.getType()) {
                Log.d(TAG, "adapter received update!");
                mHandler.post(mUpdateObjectItems);
            }
        }
    }

    private void showBitmap(PresenterItemHolder holder, Bitmap image) {
        if (holder.image.getDrawable() == null) {
            holder.image.setImageBitmap(Graphics.getInstance().getBitmapScaledToWidth(image, holder.width, holder.height));
        }
    }

    public final void setObjectItems(ArrayList<ObjectItem> newObjectItems) {
        mObjectItems = newObjectItems;
    }

    private static class PresenterItemHolder {
        RoundedImageView image;
        int width;
        int height;
    }

    private static class UpdateObjectItems implements Runnable {
        private final ObjectPresenter presenter;
        private final int mode;
        private final int type;

        public UpdateObjectItems(ObjectPresenter presenter, int mode, int type) {
            this.presenter = presenter;
            this.mode = mode;
            this.type = type;
        }

        @Override
        public void run() {
            presenter.setObjectItems(ObjectHolder.getInstance().getList(mode, type));
            presenter.notifyDataSetChanged();
        }

        public int getMode() {
            return mode;
        }

        public int getType() {
            return type;
        }

    }

}