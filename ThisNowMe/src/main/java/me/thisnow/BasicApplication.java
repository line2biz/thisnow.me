package me.thisnow;

import android.app.ActivityManager;
import android.app.Application;
import android.graphics.drawable.ColorDrawable;

import com.nostra13.universalimageloader.cache.disc.impl.TotalSizeLimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import java.io.File;

import static android.graphics.Bitmap.CompressFormat.PNG;
import static com.nostra13.universalimageloader.core.assist.ImageScaleType.EXACTLY;

public final class BasicApplication extends Application {
    private static final String IMAGES = "images";
    private static final int BYTE_FACTOR = 1024;
    private static final int DISK_CACHE_LIMIT = 50;
    private static final int POOL_SIZE = 5;
    private static final int PRIORITY = Thread.MIN_PRIORITY + 3;
    private static final int TIME_FACTOR = 1000;
    private static final int CONNECTION_TIMEOUT = 5 * TIME_FACTOR * TIME_FACTOR;
    private static final int RESPONSE_TIMEOUT = 20 * TIME_FACTOR * TIME_FACTOR;

    @Override
    public void onCreate() {
        super.onCreate();
        ImageLoader.getInstance().init(getConfig());
    }

    private DisplayImageOptions getDisplayImageOptions() {
        return new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .imageScaleType(EXACTLY)
                .build();
    }

    private ImageLoaderConfiguration getConfig() {
        return new ImageLoaderConfiguration.Builder(getApplicationContext())
                .threadPoolSize(POOL_SIZE)
                .threadPriority(PRIORITY)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new UsingFreqLimitedMemoryCache(getMemoryCacheSize()))
                .discCache(new TotalSizeLimitedDiscCache(initCacheDir(), getDirectoryCacheSize()))
                .imageDownloader(new BaseImageDownloader(getApplicationContext(), CONNECTION_TIMEOUT, RESPONSE_TIMEOUT))
                .defaultDisplayImageOptions(getDisplayImageOptions())
                .writeDebugLogs()
                .build();
    }

    private File initCacheDir() {
        File cacheDir = new File(getFilesDir(), IMAGES);
        if (!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
        return cacheDir;
    }

    private int getMemoryCacheSize() {
        ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        return (am.getMemoryClass() * BYTE_FACTOR * BYTE_FACTOR) / 2;
    }

    private int getDirectoryCacheSize() {
        return DISK_CACHE_LIMIT * BYTE_FACTOR * BYTE_FACTOR;
    }

}