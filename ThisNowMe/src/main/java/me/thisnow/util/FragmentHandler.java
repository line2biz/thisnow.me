package me.thisnow.util;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;

import static android.app.FragmentManager.POP_BACK_STACK_INCLUSIVE;

public final class FragmentHandler {
    private final FragmentManager manager;
    private final int holderId;

    public FragmentHandler(FragmentManager manager, int holderId) {
        this.manager = manager;
        this.holderId = holderId;
    }

    private void appendArguments(Fragment fragment, Bundle args) {
        if (args != null) {
            fragment.setArguments(args);
        }
    }

    public final void initCommit(Fragment fragment, Bundle args) {
        appendArguments(fragment, args);
        manager.beginTransaction()
                .add(holderId, fragment)
                .commit();
    }

    public final void setPrimary(Fragment fragment, Bundle args) {
        appendArguments(fragment, args);
        manager.beginTransaction()
                .remove(manager.findFragmentById(holderId))
                .add(holderId, fragment)
                .commit();
    }

    public final void setPrimaryAfterImmediateInclusivePopBack(Fragment fragment, Bundle args, String tag) {
        immediateInclusivePopBack(tag);
        setPrimary(fragment, args);
    }

    public final void commit(Fragment fragment, Bundle args, String backStackTag) {
        appendArguments(fragment, args);
        manager.beginTransaction()
                .replace(holderId, fragment)
                .addToBackStack(backStackTag)
                .commit();
    }

    public final Fragment getCurrent() {
        return manager.findFragmentById(holderId);
    }

    public final void immediateInclusivePopBack(String tag) {
        manager.popBackStackImmediate(tag, POP_BACK_STACK_INCLUSIVE);
    }

    public final void popBack(String tag) {
        manager.popBackStack(tag, 0);
    }

    public final void sendActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment currentFragment = getCurrent();
        if (currentFragment != null) {
            currentFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

}