package me.thisnow.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

import java.util.ArrayList;

import me.thisnow.R;
import me.thisnow.service.LocationProviderHandler;

import static android.app.Activity.RESULT_OK;
import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.KITKAT;
import static android.provider.Settings.Secure.LOCATION_MODE;
import static android.provider.Settings.Secure.LOCATION_PROVIDERS_ALLOWED;
import static android.text.format.DateUtils.SECOND_IN_MILLIS;
import static com.google.android.gms.common.ConnectionResult.SUCCESS;
import static com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import static com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import static com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY;

public final class FusedLocation implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {
    private static final String TAG = FusedLocation.class.getSimpleName();
    private static final FusedLocation INSTANCE = new FusedLocation();
    public static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 0x9000;
    private ArrayList<UpdateListener> listeners = new ArrayList<UpdateListener>();
    private LocationClient mLocationClient = null;
    private Activity mActivity = null;
    private boolean mGooglePlayServicesAvailable = false;

    private FusedLocation() {

    }

    public static FusedLocation getInstance() {
        return INSTANCE;
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "connected!");
        mLocationClient.requestLocationUpdates(buildLocationRequest(), this);
    }

    @Override
    public void onDisconnected() {
        Log.d(TAG, "disconnected, reconnect required!");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        try {
            if (mActivity != null && connectionResult.hasResolution()) {
                connectionResult.startResolutionForResult(mActivity, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } else {
                showErrorDialog(connectionResult.getErrorCode());
            }
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null && location.getAccuracy() < Options.CONFIDENCE_ACCURACY) {
            if (listeners != null && listeners.size() > 0) {
                Log.d(TAG, "update: " + location.toString());
                for (int i = 0; i < listeners.size(); i++) {
                    listeners.get(i).onChange(location);
                }
            }
        }
    }

    @TargetApi(KITKAT)
    @SuppressWarnings("deprecation")
    private boolean locationProviderEnabled(Context context) {
        return (SDK_INT >= KITKAT) ? (Settings.Secure.getInt(context.getContentResolver(), LOCATION_MODE, 0) != 0)
                : !TextUtils.isEmpty(Settings.Secure.getString(context.getContentResolver(), LOCATION_PROVIDERS_ALLOWED));
    }

    private void initLocationClient(Context context) {
        if (mLocationClient == null) {
            mLocationClient = new LocationClient(context, this, this);
        } else {
            mLocationClient.disconnect();
        }
    }

    private void removeLocationUpdatesListener() {
        if (mLocationClient.isConnected()) {
            mLocationClient.removeLocationUpdates(this);
        }
    }

    private LocationRequest buildLocationRequest() {
        return LocationRequest.create()
                .setPriority(PRIORITY_HIGH_ACCURACY)
                .setSmallestDisplacement(Options.DISTANCE_BETWEEN_LOCATION_UPDATES)
                .setInterval(Options.LOCATION_UPDATE_INTERVAL)
                .setFastestInterval(Options.LOCATION_FASTEST_UPDATE_INTERVAL);
    }

    private void showErrorDialog(int errorCode) {
        if (mActivity != null) {
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(errorCode, mActivity, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            if (errorDialog != null) {
                Log.d(TAG, "dialog not null then show!");
                new Error(errorDialog).show(mActivity.getFragmentManager(), TAG);
            }
        }
    }

    public final void addListener(UpdateListener listener) {
        if (listeners != null) {
            listeners.add(listener);
        }
    }

    public final void removeListener(UpdateListener listener) {
        if (listeners != null) {
            listeners.remove(listener);
        }
    }

    public final void startProviderService(Context context) {
        context.startService(new Intent(context, LocationProviderHandler.class));
    }

    public final void stopProviderService(Context context) {
        context.stopService(new Intent(context, LocationProviderHandler.class));
    }

    public final void setActivity(Activity activity) {
        this.mActivity = activity;
    }

    public final Activity getActivity() {
        return mActivity;
    }

    public final void checkGooglePlayServices(Context context) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        mGooglePlayServicesAvailable = (SUCCESS == resultCode);
        if (!mGooglePlayServicesAvailable) {
            Log.d(TAG, "google play services not available, show error dialog");
            showErrorDialog(resultCode);
        }
    }

    public final void connect(Context context, UpdateListener listener) {
        if (mGooglePlayServicesAvailable) {
            if (locationProviderEnabled(context)) {
                initLocationClient(context);
                mLocationClient.connect();
                addListener(listener);
            } else {
                Message.show(context, R.string.error_location_provider_not_available);
            }
        }
    }

    public final void disconnect(UpdateListener listener) {
        if (mGooglePlayServicesAvailable) {
            if (mLocationClient != null) {
                removeLocationUpdatesListener();
                mLocationClient.disconnect();
                removeListener(listener);
            }
        }
    }

    public final void handleResolutionResult(Context context, int resultCode) {
        switch (resultCode) {
            case RESULT_OK:
                checkGooglePlayServices(context);
                break;
            default:
                Message.show(context, R.string.error_google_play_services_not_available);
                break;
        }
    }

    private interface Options {
        long LOCATION_UPDATE_INTERVAL = 5 * SECOND_IN_MILLIS;
        long LOCATION_FASTEST_UPDATE_INTERVAL = 1 * SECOND_IN_MILLIS;
        float DISTANCE_BETWEEN_LOCATION_UPDATES = 50.0f;
        float CONFIDENCE_ACCURACY = 50.0f;
    }

    public interface UpdateListener {
        void onChange(Location location);
    }

    private static class Error extends DialogFragment {

        private final Dialog mDialog;

        public Error(Dialog dialog) {
            super();
            this.mDialog = dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }

    }

}