package me.thisnow.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class Encrypt {
    private static final String MD5 = "MD5";

    public static String md5(String input) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance(MD5);
            digest.reset();
            digest.update(input.getBytes());
            byte[] encrypted = digest.digest();
            int len = encrypted.length;
            StringBuilder builder = new StringBuilder(len << 1);
            for (int i = 0; i < len; i++) {
                builder.append(Character.forDigit((encrypted[i] & 0xf0) >> 4, 16));
                builder.append(Character.forDigit(encrypted[i] & 0x0f, 16));
            }
            return builder.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}