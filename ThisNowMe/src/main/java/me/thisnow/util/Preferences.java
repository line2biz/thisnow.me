package me.thisnow.util;

import android.content.Context;

import static android.content.Context.MODE_PRIVATE;
import static me.thisnow.util.Preferences.Key.ACCOUNT_TOKEN;
import static me.thisnow.util.Preferences.Key.APP_INFO;

public final class Preferences {
    private static final String BASE = Preferences.class.getCanonicalName();

    public static void setAppInfoShown(Context context, boolean value) {
        context.getSharedPreferences(BASE, MODE_PRIVATE).edit().putBoolean(APP_INFO, value).commit();
    }

    public static void setAccountToken(Context context, String accountToken) {
        context.getSharedPreferences(BASE, MODE_PRIVATE).edit().putString(ACCOUNT_TOKEN, accountToken).commit();
    }

    public static boolean checkAppInfoShown(Context context) {
        return context.getSharedPreferences(BASE, MODE_PRIVATE).getBoolean(APP_INFO, false);
    }

    public static String getAccountToken(Context context) {
        return context.getSharedPreferences(BASE, MODE_PRIVATE).getString(ACCOUNT_TOKEN, null);
    }

    interface Key {
        String BASE = Key.class.getCanonicalName();
        String APP_INFO = BASE + ".App.Info";
        String ACCOUNT_TOKEN = BASE + ".Account.Token";
    }

}