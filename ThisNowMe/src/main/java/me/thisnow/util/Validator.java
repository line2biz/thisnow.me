package me.thisnow.util;

import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public final class Validator {

    public boolean notEmpty(EditText field, String error) {
        String value = field.getText().toString();
        if (value == null || value.isEmpty()) {
            field.setError(error);
        }
        return (value != null && !value.isEmpty());
    }

    public boolean notNull(View view) {
        return (view != null);
    }

    public boolean notNull(Bitmap bitmap) {
        return (bitmap != null);
    }

    public boolean notNull(String value) {
        return (value != null);
    }

    public boolean notNull(EditText field, String error) {
        return notNull(field, field.getText().toString(), error);
    }

    public boolean notNull(EditText field, String value, String error) {
        if (value == null) {
            field.setError(error);
        }
        return (value != null);
    }

    public boolean childCountNotEqual(ViewGroup parent, int count) {
        return (parent.getChildCount() != count);
    }

}
