package me.thisnow.util;

import android.content.Context;
import android.widget.Toast;

public final class Message {

    public static void show(Context context, int messageId) {
        show(context, context.getString(messageId));
    }

    public static void show(Context context, String message) {
        if (message != null && !message.isEmpty()) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

}