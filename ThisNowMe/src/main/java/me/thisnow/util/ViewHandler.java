package me.thisnow.util;

import android.view.View;
import android.widget.AbsListView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.PauseOnScrollListener;

import static android.view.View.GONE;
import static android.view.View.OnClickListener;
import static android.view.View.VISIBLE;
import static android.widget.AdapterView.OnItemClickListener;

public final class ViewHandler {
    private static final PauseOnScrollListener SCROLL_LISTENER = new PauseOnScrollListener(ImageLoader.getInstance(), true, true);

    private ViewHandler() {

    }

    public static void setClickListeners(OnClickListener listener, View... views) {
        for (View view : views) {
            view.setOnClickListener(listener);
        }
    }

    public static void setListListeners(OnItemClickListener listener, AbsListView... listViews) {
        for (AbsListView listView : listViews) {
            listView.setOnItemClickListener(listener);
            listView.setOnScrollListener(SCROLL_LISTENER);
        }
    }

    public static void show(View... views) {
        changeVisibility(VISIBLE, views);
    }

    public static void hide(View... views) {
        changeVisibility(GONE, views);
    }

    private static void changeVisibility(int state, View... views) {
        for (View view : views) {
            view.setVisibility(state);
        }
    }

}