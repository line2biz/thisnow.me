package me.thisnow.util;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Build;

import me.thisnow.constant.Char;

public final class Device {
    private static final String IMEI_BASE = "39";

    public static String wifiMACAddress(Context context) {
        //TODO return to WIFI manager result
//        return ((WifiManager) context.getSystemService(Context.WIFI_SERVICE)).getConnectionInfo().getMacAddress();
        return "00:00:00:00:00:14";
    }

    public static String pseudoIMEI() {
        return new StringBuilder(IMEI_BASE)
                .append(Build.BOARD.length() % 10)
                .append(Build.BRAND.length() % 10)
                .append(Build.CPU_ABI.length() % 10)
                .append(Build.DEVICE.length() % 10)
                .append(Build.DISPLAY.length() % 10)
                .append(Build.HOST.length() % 10)
                .append(Build.ID.length() % 10)
                .append(Build.MANUFACTURER.length() % 10)
                .append(Build.MODEL.length() % 10)
                .append(Build.PRODUCT.length() % 10)
                .append(Build.TAGS.length() % 10)
                .append(Build.TYPE.length() % 10)
                .append(Build.USER.length() % 10)
                .toString();
    }

    public static String pseudoUniqueID(Context context) {
        return new StringBuilder(wifiMACAddress(context)).append(Char.COLON).append(pseudoIMEI()).toString();
    }

}