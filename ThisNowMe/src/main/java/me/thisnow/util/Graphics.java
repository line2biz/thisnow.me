package me.thisnow.util;

import android.graphics.Bitmap;
import android.graphics.Matrix;

import static android.graphics.Bitmap.Config.ALPHA_8;

public class Graphics {
    private static final Graphics INSTANCE = new Graphics();
    private static final float ACTUAL_SCALE = 1.0f;

    private Graphics() {

    }

    public static Graphics getInstance() {
        return INSTANCE;
    }

    private float getScaleToFitWidth(int requiredWidth, int actualWidth, int actualHeight) {
        if (actualWidth > actualHeight) {
            return ((float) requiredWidth / actualHeight);
        } else {
            return ((float) requiredWidth / actualWidth);
        }
    }

    private float getScaleForSize(int actualSize, int requiredSize) {
        return ((float) requiredSize / actualSize);
    }

    private Bitmap getTransparentBitmap(int width, int height) {
        return Bitmap.createBitmap(width, height, ALPHA_8);
    }

    private Bitmap getSquareScaledBitmap(Bitmap bitmap, int width, int height, float scale) {
        final int actualSize = getFitSize(width, height);
        return getScaledBitmap(bitmap, actualSize, actualSize, scale, scale);
    }

    private int getFitSize(int width, int height) {
        return (width > height) ? height : width;
    }

    private Matrix getScaleMatrix(float widthScale, float heightScale) {
        final Matrix matrix = new Matrix();
        matrix.postScale(widthScale, heightScale);
        return matrix;
    }

    private Bitmap getScaledBitmap(Bitmap bitmap, int width, int height, float widthScale, float heightScale) {
        if (widthScale == ACTUAL_SCALE && heightScale == ACTUAL_SCALE) {
            return bitmap;
        } else {
            return Bitmap.createBitmap(bitmap, 0, 0, width, height, getScaleMatrix(widthScale, heightScale), true);
        }
    }

    public final Bitmap getSquareBitmapScaledToWidth(Bitmap image, int requiredWidth) {
        if (image != null) {
            return getSquareScaledBitmap(image, image.getWidth(), image.getHeight(), getScaleToFitWidth(requiredWidth, image.getWidth(), image.getHeight()));
        } else {
            return getTransparentBitmap(requiredWidth, requiredWidth);
        }
    }

    public final Bitmap getBitmapScaledToWidth(Bitmap image, int requiredWidth, int requiredHeight) {
        if (image != null) {
            return getScaledBitmap(image, image.getWidth(), image.getHeight(), getScaleForSize(image.getWidth(), requiredWidth), getScaleForSize(image.getHeight(), requiredHeight));
        } else {
            return getTransparentBitmap(requiredWidth, requiredHeight);
        }
    }

}