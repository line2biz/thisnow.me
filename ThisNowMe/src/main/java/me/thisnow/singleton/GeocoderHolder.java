package me.thisnow.singleton;

import java.util.ArrayList;

import me.thisnow.service.model.geocoding.Result;

public final class GeocoderHolder {
    private static final GeocoderHolder INSTANCE = new GeocoderHolder();
    private ArrayList<Result> results = new ArrayList<Result>();

    private GeocoderHolder() {

    }

    public static GeocoderHolder getInstance() {
        return INSTANCE;
    }

    public ArrayList<Result> getResults() {
        return results;
    }

    public void setResults(ArrayList<Result> results) {
        this.results = results;
    }

}