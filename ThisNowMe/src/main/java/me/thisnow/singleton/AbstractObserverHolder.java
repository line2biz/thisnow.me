package me.thisnow.singleton;

import java.util.ArrayList;

abstract class AbstractObserverHolder {
    private final ArrayList<Observer> mObservers = new ArrayList<Observer>();

    public void registerObserver(Observer observer) {
        mObservers.add(observer);
    }

    public void unregisterObserver(Observer observer) {
        mObservers.remove(observer);
    }

    protected void notifyObservers(int key, int value) {
        if (mObservers != null && mObservers.size() != 0) {
            for (int i = 0; i < mObservers.size(); i++) {
                mObservers.get(i).onUpdate(key, value);
            }
        }
    }

    public interface Observer {
        void onUpdate(int key, int value);
    }

}