package me.thisnow.singleton;

public final class AccountInfoHolder {
    private static final AccountInfoHolder INSTANCE = new AccountInfoHolder();
    private String token;
    private String objectId;
    private int objectType;

    private AccountInfoHolder() {

    }

    public static AccountInfoHolder getInstance() {
        return INSTANCE;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public int getObjectType() {
        return objectType;
    }

    public void setObjectType(int objectType) {
        this.objectType = objectType;
    }

}