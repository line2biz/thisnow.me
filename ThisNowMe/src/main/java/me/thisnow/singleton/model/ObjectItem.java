package me.thisnow.singleton.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class ObjectItem {
    @Expose
    @SerializedName("object_id")
    private String objectId;
    @Expose
    private String image;

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}