package me.thisnow.singleton;

public final class Settings {
    private static final Settings INSTANCE = new Settings();
    private int radius = 100;

    private Settings() {

    }

    public static Settings getInstance() {
        return INSTANCE;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

}