package me.thisnow.singleton;

import android.content.Context;

import java.util.ArrayList;

import me.thisnow.R;
import me.thisnow.singleton.model.ObjectItem;

import static android.graphics.Color.WHITE;
import static me.thisnow.service.constant.Mode.AROUND_ME;
import static me.thisnow.service.constant.Mode.TIMELINE;
import static me.thisnow.service.constant.Type.BUSINESS;
import static me.thisnow.service.constant.Type.EVENT;
import static me.thisnow.service.constant.Type.PERSON;

public final class ObjectHolder extends AbstractObserverHolder {
    private static final ObjectHolder INSTANCE = new ObjectHolder();

    private final ArrayList<ObjectItem> mTimelinePersonObjects = new ArrayList<ObjectItem>();
    private final ArrayList<ObjectItem> mTimelineBusinessObjects = new ArrayList<ObjectItem>();
    private final ArrayList<ObjectItem> mTimelineEventObjects = new ArrayList<ObjectItem>();

    private final ArrayList<ObjectItem> mAroundMePersonObjects = new ArrayList<ObjectItem>();
    private final ArrayList<ObjectItem> mAroundMeBusinessObjects = new ArrayList<ObjectItem>();
    private final ArrayList<ObjectItem> mAroundMeEventObjects = new ArrayList<ObjectItem>();

    private ObjectHolder() {

    }

    public static ObjectHolder getInstance() {
        return INSTANCE;
    }

    public final void setList(ArrayList<ObjectItem> newObjects, int mode, int type) {
        ArrayList<ObjectItem> holder = getList(mode, type);
        holder.clear();
        holder.addAll(newObjects);
        notifyObservers(mode, type);
    }

    public final void addObject(ObjectItem newObject, int mode, int type) {
        getList(mode, type).add(newObject);
        notifyObservers(mode, type);
    }

    public final void addObject(ObjectItem newObject, int position, int mode, int type) {
        getList(mode, type).add(position, newObject);
        notifyObservers(mode, type);
    }

    public final void addObjects(ArrayList<ObjectItem> newObjects, int mode, int type) {
        getList(mode, type).addAll(newObjects);
        notifyObservers(mode, type);
    }

    public final void addObjects(ArrayList<ObjectItem> newObjects, int position, int mode, int type) {
        getList(mode, type).addAll(position, newObjects);
        notifyObservers(mode, type);
    }

    public final ArrayList<ObjectItem> getList(int mode, int type) {
        if (mode == TIMELINE && type == PERSON) {
            return mTimelinePersonObjects;
        } else if (mode == TIMELINE && type == BUSINESS) {
            return mTimelineBusinessObjects;
        } else if (mode == TIMELINE && type == EVENT) {
            return mTimelineEventObjects;
        } else if (mode == AROUND_ME && type == PERSON) {
            return mAroundMePersonObjects;
        } else if (mode == AROUND_ME && type == BUSINESS) {
            return mAroundMeBusinessObjects;
        } else if (mode == AROUND_ME && type == EVENT) {
            return mAroundMeEventObjects;
        } else {
            return null;
        }
    }

    public final int getBorderColor(Context context, int type) {
        switch (type) {
            case PERSON:
                return context.getResources().getColor(R.color.app_vinous);
            case EVENT:
                return context.getResources().getColor(R.color.app_yellow);
            case BUSINESS:
                return context.getResources().getColor(R.color.app_green);
            default:
                return WHITE;
        }
    }

}