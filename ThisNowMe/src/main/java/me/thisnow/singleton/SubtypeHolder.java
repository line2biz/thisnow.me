package me.thisnow.singleton;

import java.util.ArrayList;

import me.thisnow.service.constant.Type;
import me.thisnow.service.model.Subtype;

public final class SubtypeHolder {
    private static final SubtypeHolder INSTANCE = new SubtypeHolder();
    private ArrayList<Subtype> mPersonSubtypes = new ArrayList<Subtype>();
    private ArrayList<Subtype> mBusinessSubtypes = new ArrayList<Subtype>();
    private ArrayList<Subtype> mEventSubtypes = new ArrayList<Subtype>();

    private SubtypeHolder() {

    }

    public static SubtypeHolder getInstance() {
        return INSTANCE;
    }

    public void setSubtypes(int type, ArrayList<Subtype> items) {
        ArrayList<Subtype> subtypes = getList(type);
        subtypes.clear();
        subtypes.addAll(items);
    }

    public ArrayList<Subtype> getSubtypes(int type) {
        return getList(type);
    }

    public int getSubtypeCount(int type) {
        return getList(type).size();
    }

    public void addSubtype(int type, Subtype item) {
        getList(type).add(item);
    }

    public void removeSubtype(int type, int position) {
        getList(type).remove(position);
    }

    private ArrayList<Subtype> getList(int type) {
        switch (type) {
            case Type.PERSON:
                return mPersonSubtypes;
            case Type.BUSINESS:
                return mBusinessSubtypes;
            case Type.EVENT:
                return mEventSubtypes;
        }
        return null;
    }

}