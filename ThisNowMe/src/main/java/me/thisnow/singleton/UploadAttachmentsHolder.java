package me.thisnow.singleton;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import static android.provider.BaseColumns._ID;
import static me.thisnow.database.model.Attachment.CONTENT_URI;
import static me.thisnow.database.model.Attachment.OBJECT_ID;
import static me.thisnow.singleton.UploadAttachmentsHolder.Key.CURRENT;
import static me.thisnow.singleton.UploadAttachmentsHolder.Key.MAX;

public final class UploadAttachmentsHolder extends AbstractObserverHolder {
    private static final String TAG = UploadAttachmentsHolder.class.getSimpleName();
    private static final UploadAttachmentsHolder INSTANCE = new UploadAttachmentsHolder();
    private int maxCount = 0;
    private int currentPosition = 0;
    private boolean stopHandler = false;

    private UploadAttachmentsHolder() {

    }

    public static UploadAttachmentsHolder getInstance() {
        return INSTANCE;
    }

    public int getMax() {
        return maxCount;
    }

    public void setMax(int value) {
        maxCount = value;
        notifyObservers(MAX, maxCount);
    }

    public int getCurrent() {
        return currentPosition;
    }

    public void setCurrent(int value) {
        currentPosition = value;
        notifyObservers(CURRENT, currentPosition);
    }

    public Cursor getAll(Context context) {
        Log.d(TAG, "upload: get all attachments!");
        return context.getContentResolver().query(CONTENT_URI, null, OBJECT_ID + "=?",
                new String[]{AccountInfoHolder.getInstance().getObjectId()}, null);
    }

    public void deleteAll(Context context) {
        Log.d(TAG, "upload: delete all attachments!");
        context.getContentResolver().delete(CONTENT_URI, OBJECT_ID + "=?",
                new String[]{AccountInfoHolder.getInstance().getObjectId()});
    }

    public void insert(Context context, ContentValues values) {
        Log.d(TAG, "upload: insert attachment!");
        context.getContentResolver().insert(CONTENT_URI, values);
    }

    public void deleteById(Context context, long id) {
        Log.d(TAG, "upload: delete attachments by ID: " + id);
        context.getContentResolver().delete(CONTENT_URI, _ID + "=?", new String[]{String.valueOf(id)});
    }

    public boolean getHandlerStopState() {
        return stopHandler;
    }

    public void setHandlerStopState(boolean state) {
        Log.d(TAG, "upload: handler stop state: " + true);
        stopHandler = state;
    }

    public interface Key {
        int MAX = 0x7007;
        int CURRENT = 0x7008;
    }

}